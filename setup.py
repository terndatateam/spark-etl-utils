import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

with open("requirements.txt") as req:
    install_requires = req.read().split("\n")

setuptools.setup(
    name="spark-etl-utils",
    version="2.6.3",
    author="Javier Sanchez Gonzalez",
    author_email="j.sanchezgonzalez@uq.edu.au",
    description="A Python library with multiple classes and methods common to several TERN Spark ETL programs.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://bitbucket.org/terndatateam/spark-etl-utils/src",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "Operating System :: OS Independent",
    ],
    install_requires=install_requires,
)
