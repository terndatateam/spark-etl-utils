import os
import uuid
from urllib import parse

from tern_rdf import TernRdf

from .coordinates import utm_to_lat_long
from .custom_accumulators import ListAccumulator, RDFGraphAccumulator, add_error
from .transform import Transform
from .validation import *


def write_to_disk(
    g: Graph, app_dir: str, output_dir: str, table_name: str, rdf_format="turtle"
) -> None:
    output_dir = os.path.join(app_dir, output_dir)
    if not os.path.isdir(output_dir):
        os.mkdir(output_dir)

    file_extension = TernRdf.formats.get(rdf_format)
    if file_extension is None:
        raise RuntimeError(
            "The selected format '{}' is not available for serializing. Built in formats are: "
            "'xml', 'n3', 'turtle', 'nt', 'pretty-xml', 'trix', 'trig' and 'nquads'.".format(
                rdf_format
            )
        )
    else:
        g.serialize(
            "{}-{}{}".format(os.path.join(output_dir, table_name), uuid.uuid4(), file_extension),
            format=rdf_format,
        )
        print("Length of {} graph: {}".format(table_name, len(g)))


def url_encode(url):
    """
    Use the URLLIB module to 'url encode' the string/URL
    :param url:
    :return: URL Encoded string/URL
    """
    return parse.quote_plus(str(url))


def clean_string(string):
    return (
        string.translate({ord(c): "-" for c in " !@#$%^&*()[]{};:,./<>?\|`~=_+"})
        if string
        else None
    )
