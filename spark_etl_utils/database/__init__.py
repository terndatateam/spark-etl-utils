import psycopg2 as pg
import pandas
from pyspark.sql import DataFrame
from pyspark.sql.session import SparkSession


def get_postgres_url(host, port, dbname, username=None, password=None):
    """
    Build a postgres DB URI as a String
    :param host: DB host
    :param port: DB port
    :param dbname: DB database
    :param username: DB username
    :param password: DB password
    :return: URI as a String
    """
    if username is not None and password is not None:
        return "jdbc:postgresql://{}:{}/{}?user={}&password={}".format(host, port, dbname, username, password)
    else:
        return "jdbc:postgresql://{}:{}/{}".format(host, port, dbname)


def get_db_table_pg(spark: SparkSession, host: str, port: str, dbname: str, table_name: str, user: str, password: str,
                    schema: str = "public") -> pandas.DataFrame:
    """
    Get a Pandas DataFrame object from a remote database table.

    :param schema:
    :param table_name: Table name
    :param dbname: Database name
    :param user: Username
    :param password: Password
    :param host: Host
    :param port: Port
    :return: Spark DataFrame
    """

    url = get_postgres_url(host, port, dbname, user, password)
    df = spark.read \
        .format("jdbc") \
        .option("url", url) \
        .option("dbtable", "{}.{}".format(schema, table_name)) \
        .option("driver", "org.postgresql.Driver") \
        .load()

    return df


def get_db_query_pg(spark: SparkSession, host: str, port: str, dbname: str, query: str, user: str,
                    password: str) -> pandas.DataFrame:
    """
    Get a Pandas DataFrame object from a remote database table.

    :param schema:
    :param table_name: Table name
    :param dbname: Database name
    :param user: Username
    :param password: Password
    :param host: Host
    :param port: Port
    :return: Spark DataFrame
    """

    url = get_postgres_url(host, port, dbname, user, password)
    df = spark.read \
        .format("jdbc") \
        .option("url", url) \
        .option("dbtable", query) \
        .option("driver", "org.postgresql.Driver") \
        .load()

    return df


def get_db_table_pandas(host: str, port: str, dbname: str, table_name: str, user: str,
                        password: str, schema: str = "public", query: str = None) -> pandas.DataFrame:
    """
    Get a Pandas DataFrame object from a remote database table.

    :param schema:
    :param table_name: Table name
    :param dbname: Database name
    :param user: Username
    :param password: Password
    :param host: Host
    :param port: Port
    :param query: Specific query
    :return: Pandas DataFrame
    """

    if not query:
        query = 'select * from {}.{}'.format(schema, table_name)

    conn = pg.connect(host=host, port=port, dbname=dbname, user=user, password=password)
    df = pandas.io.sql.read_sql(query, conn, coerce_float=False)

    return df


def get_db_table_jdbc(spark: SparkSession, url: str, dbname: str, table_name: str, custom_schema: str) -> DataFrame:
    """
    Get a Spark DataFrame object from a remote database table.

    :param spark: SparkSession
    :param url: JDBC url
    :param dbname: Database name
    :param table_name: Table name
    :param custom_schema: Spark SQL customSchema
    :return: Spark DataFrame
    """
    df = spark.read \
        .format('jdbc') \
        .options(url=url, database=dbname, dbtable=table_name) \
        .option('customSchema', custom_schema) \
        .load()

    return df


def load_db_table_jdbc(spark: SparkSession, url: str, dbname: str, table_name: str) -> DataFrame:
    """
    Get a Spark DataFrame object from a remote database table.

    :param spark: SparkSession
    :param url: JDBC url
    :param dbname: Database name
    :param table_name: Table name
    :param custom_schema: Spark SQL customSchema
    :return: Spark DataFrame
    """
    df = spark.read \
        .format('jdbc') \
        .options(url=url, dbtable=table_name, driver="org.h2.Driver") \
        .load()

    return df


def save_db_table_jdbc(df: DataFrame, url: str, table_name: str, username: str, password: str):
    df.write.jdbc(url, table_name, properties={"user": username, "password": password})


def save_dataframe_as_jdbc_table(df: DataFrame, db_url: str, dataset: str, table_name: str,
                                 save_mode: str = "overwrite"):
    df.write.mode(save_mode).jdbc(db_url, "{}.{}".format(dataset, table_name))
