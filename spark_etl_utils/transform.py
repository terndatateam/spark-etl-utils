import os
import itertools
from abc import ABC

from pyspark import Accumulator, Broadcast

from spark_etl_utils.database import get_db_table_pandas
from spark_etl_utils.validation import clean_columns

VOC_MAPPINGS_QUERY = """select * from {}.r_vocabulary_mappings where db_table='{}'"""

YB_MAPPINGS_QUERY = 'select "dataset_concept_label", "dataset_concept_uri", "global_concept_uri", ' \
                    '"global_collection_uri" from {}.r_yb_vocabulary_mappings'


class Transform(ABC):
    """
    Base abstract class for transforming tabular data into RDF.
    """
    dataset = None
    table = None
    spark = None
    df = None

    def __init__(self):
        self.db_host = os.getenv('POSTGRES_HOST')
        self.db_port = os.getenv('POSTGRES_PORT')
        self.db_name = os.getenv('POSTGRES_DB')
        self.db_username = os.getenv('POSTGRES_USER')
        self.db_password = os.getenv('POSTGRES_PASSWORD')

    def clean(self):
        """
        Apply cleaning functions to the data.
        """
        self.df = clean_columns(self.df)

    def validate(self) -> None:
        """
        Apply validation processes here in the child class.
        """
        pass

    def load_vocabulary_mappings(self) -> Broadcast:
        df_vocab_mappings = get_db_table_pandas(
            self.db_host,
            self.db_port,
            self.db_name,
            'r_vocabulary_mappings',
            self.db_username,
            self.db_password,
            self.dataset,
            query=VOC_MAPPINGS_QUERY.format(self.dataset, self.table)
        )
        return self.spark.sparkContext.broadcast(df_vocab_mappings)

    def load_lookup(self):
        pass

    def derive_data_products(self) -> None:
        """
        Performs and saves into db calculations for derived TERN data products
        """
        pass

    def pre_transform(self, errors: Accumulator, warnings: Accumulator) -> None:
        """
        Performs calculations and saves result into db
        """
        pass

    def clean_nulls(self) -> None:
        """
        Clean-up (homogenize data cell values that mean NULL)
        """
        pass

    @staticmethod
    def transform(rows: itertools.chain, dataset: str, namespace_url: str, table_name: str,
                  vocabulary_mappings: Broadcast, vocabulary_graph: Broadcast, errors: Accumulator,
                  warnings: Accumulator, lookup: Broadcast = None, ontology: Broadcast = None):
        """
        The function passed to a DataFrame's foreach method to perform data transformation to RDF. Override this.
        """
        pass
