from rdflib import DCAT, DCTERMS, RDF, BNode, Graph, Literal, Namespace, URIRef
from rdflib.container import Seq
from rdflib.graph import Seq as SeqRead
from tern_rdf.namespace_bindings import SCHEMA, TERN, TernRdf

from spark_etl_utils import write_to_disk

GEONETWORK_BACKEND_URL = (
    "https://geonetwork.tern.org.au/geonetwork/srv/eng/rdf.metadata.get?uuid={0}&approved=true"
)
TDDP_URL = "https://portal.tern.org.au/metadata/TERN/{}"

ISO_ROLES = [
    "resourceProvider",
    "custodian",
    "owner",
    "user",
    "distributor",
    "originator",
    "principalInvestigator",
    "processor",
    "sponsor",
    "coAuthor",
    "collaborator",
    "editor",
    "mediator",
    "rightsHolder",
    "contributor",
    "funder",
    "stakeholder",
]


def fix_namespace(namespace: Namespace):
    if namespace.__str__()[-1:] == "/":
        return Namespace(namespace.__str__()[:-1])
    else:
        return namespace


def get_party_from_metadata_record(
    dataset, dataset_namespace, record_uri, output, output_format, output_g=None
):
    # Get RDF from geonetwork
    g = Graph()
    g.parse(GEONETWORK_BACKEND_URL.format(record_uri))

    if output_g is None:
        # Create output graph
        output_g = TernRdf.Graph(dataset)
        new_graph = True
    else:
        new_graph = False

    # Find CatalogRecord and DCAT.landingPage
    for record in g.subjects(RDF.type, DCAT.CatalogRecord):
        for record_id in g.objects(record, DCTERMS.identifier):
            output_g.add(
                (
                    URIRef(dataset_namespace),
                    DCAT.landingPage,
                    Literal(TDDP_URL.format(record_id)),
                )
            )

    # Find the datasets in Graph
    for dataset in g.subjects(RDF.type, DCAT.Dataset):
        # For each role (using the predicate associated to each role), add the Parties
        # (Person/Organization) to the output graph
        find_and_add_party_by_role(g, output_g, dataset_namespace, dataset, DCTERMS.publisher)
        find_and_add_party_by_role(g, output_g, dataset_namespace, dataset, DCTERMS.creator)
        find_and_add_party_by_role(g, output_g, dataset_namespace, dataset, DCAT.contactPoint)
        for role in ISO_ROLES:
            find_and_add_party_by_role(
                g, output_g, dataset_namespace, dataset, TERN.__getattr__(role)
            )

    if new_graph:
        # Print RDF file
        write_to_disk(output_g, "", output, "party_role_citation", rdf_format=output_format)

    return output_g


def find_and_add_party_by_role(g, output_g, dataset_namespace, dataset, role):
    # Author and coAuthor are modelled as rdf:Seq
    if role == DCTERMS.creator or role == TERN.coAuthor:
        for _ in g.objects(dataset, role):
            sequence = g.objects(dataset, role).__next__()
            output_seq = Seq(output_g, BNode())
            for party in SeqRead(g, sequence):
                output_g += g.triples((party, RDF.type, None))
                output_g += g.triples((party, SCHEMA.name, None))
                output_seq.append(party)
            output_g.add(
                (
                    URIRef(dataset_namespace),
                    role,
                    output_seq.uri,
                )
            )
    else:
        for party in g.objects(dataset, role):
            output_g += g.triples((party, RDF.type, None))
            output_g += g.triples((party, SCHEMA.name, None))
            output_g.add(
                (
                    URIRef(dataset_namespace),
                    role,
                    URIRef(party),
                )
            )
