import datetime
import itertools
from typing import Union, Iterator

import pandas as pd
import requests
from pyspark import Broadcast, Accumulator
from rdflib import URIRef, Graph, Literal
from rdflib.namespace import Namespace, SKOS, DCTERMS, XSD, RDF
from tern_rdf.namespace_bindings import TernRdf, TERN

from spark_etl_utils import add_error
from spark_etl_utils.rdf.models import (
    Sample,
    ObservableProperty,
    Attribute,
    Sampler,
    Text,
    Boolean,
    Observation,
    DateTime,
    RDFDataset,
    Site,
    SiteVisit,
    Date,
    IRI,
    Instant,
    FeatureOfInterest,
    generate_underscore_uri,
    MaterialSample,
    Sampling,
    Taxon,
    Integer,
    Float,
    Sensor,
    IGSN,
)
from spark_etl_utils.rdf.vocabulary_mappings import (
    get_mappings,
    get_yb_mappings_column,
    get_mappings_es,
)

UNIQUE_ID = "unique_id"
SITE_ATTR_UNIQUE_ID = "site_attr_unique_id"
VISIT_ATTR_UNIQUE_ID = "visit_attr_unique_id"
INSTR_ATTR_UNIQUE_ID = "instr_attr_unique_id"
MS_ATTR_UNIQUE_ID = "ms_attr_unique_id"
SAMPLE_ATTR_UNIQUE_ID = "sample_attr_unique_id"
FOI_ATTR_UNIQUE_ID = "foi_attr_unique_id"
SITE_LOCATION_ID = "site_id"
SITE_LOCATION_VISIT_ID = "site_visit_id"
DEFAULT_DATETIME = "default_datetime"

RECORD_TYPE_OBSERVATION = "observation"
RECORD_TYPE_ATTRIBUTE = "attribute"

SITE_TYPE_PLOT = "http://linked.data.gov.au/def/tern-cv/8cadf069-01d7-4420-b454-cae37740c2a2"
SITE_TYPE_SITE = "http://linked.data.gov.au/def/tern-cv/5bf7ae21-a454-440b-bdd7-f2fe982d8de4"
SITE_TYPE_TRANSECT = "http://linked.data.gov.au/def/tern-cv/de46fa49-d1c9-4bef-8462-d7ee5174e1e1"
SITE_TYPE_QUADRAT = "http://linked.data.gov.au/def/tern-cv/4362c8f2-b3cc-4816-b5a2-fb7bb4c0cff5"
SITE_TYPE_PARENT_SITE = "http://linked.data.gov.au/def/tern-cv/ffbe3c8c-23f1-4fc4-8aaf-dfba9f12576c"

TERN_CV_PREFIX = "http://linked.data.gov.au/def/tern-cv/"
TERN_CV_SHORT = "tern:"
REGION_TYPE_PREFIX = "http://linked.data.gov.au/dataset/"
REGION_TYPE_SHORT = "region:"

SW_POINT_TYPE = "http://linked.data.gov.au/def/tern-cv/4dd22567-d692-44e7-b29d-b80eaff829dc"
CENTROID_POINT_TYPE = "http://linked.data.gov.au/def/tern-cv/7e2e87b6-c9ec-43ac-92b0-1976fc623c0c"
TRANSECT_START_POINT = "http://linked.data.gov.au/def/tern-cv/e0844654-01a0-44f9-a996-b09a10013ceb"
TRANSECT_END_POINT = "http://linked.data.gov.au/def/tern-cv/3dcb67d8-eb2f-4063-b6da-8521cc1ca5bc"


def get_vocabulary_concept(
    vocabs: Graph, collection_uri: URIRef, predicate_uri: URIRef, value_to_match
):
    for concept in vocabs.objects(collection_uri, SKOS.member):
        for v in vocabs.objects(concept, predicate_uri):
            if str(v).lower() == str(value_to_match).lower():
                return concept


def get_vocabulary_concept_label(
    vocabs: Graph, collection_uri: URIRef, predicate_uri: URIRef, value_to_match
):
    for concept in vocabs.objects(collection_uri, SKOS.member):
        for v in vocabs.objects(concept, predicate_uri):
            if str(v) == str(value_to_match):
                for label in vocabs.objects(concept, SKOS.prefLabel):
                    return concept, str(label)
                return concept, None
    return None, None


def parse_vocab_from_url(g: Graph, rdf_url: str, rdf_format: str = "turtle") -> Graph:
    r = requests.get(rdf_url, params={"format": "ttl"})
    g.parse(data=r.content.decode("utf-8"), format=rdf_format)
    return g


def get_observation_dates(row, column_mapping, default_site_visit_date):
    phenomenom_time = default_site_visit_date
    result_time = default_site_visit_date
    if column_mapping["phenomenon_time_column"] and row[column_mapping["phenomenon_time_column"]]:
        phenomenom_time = row[column_mapping["phenomenon_time_column"]]
    if column_mapping["result_time_column"] and row[column_mapping["result_time_column"]]:
        result_time = row[column_mapping["result_time_column"]]
    if isinstance(phenomenom_time, datetime.date):
        phenomenom_time = datetime.datetime.combine(phenomenom_time, datetime.datetime.min.time())
    if isinstance(result_time, datetime.date):
        result_time = datetime.datetime.combine(result_time, datetime.datetime.min.time())
    return phenomenom_time, result_time


def generate_rdf_graph(
    rows: Union[itertools.chain, Iterator, pd.core.series.Series],
    datasource: str,
    ns: Namespace,
    vocab_mappings: Union[Broadcast, dict],
    vocabs_g: Union[Broadcast, Graph],
    errors: Union[Accumulator, list],
    warnings: Union[Accumulator, list],
    project_dataset_column: str = None,
):
    g = TernRdf.Graph([datasource.upper()])
    if type(rows) == pd.core.series.Series:
        vocabs = vocabs_g
        mappings = get_mappings(vocab_mappings, errors)
    else:
        vocabs = vocabs_g.value
        mappings = get_mappings(vocab_mappings.value, errors)

    for row in rows:
        site_visit_uri = None
        try:
            site_visit_uri = SiteVisit.generate_uri(ns, row[SITE_LOCATION_VISIT_ID])
        except ValueError:
            # Column SITE_LOCATION_VISIT_ID not available.
            pass

        site_uri = None
        try:
            if type(rows) == pd.core.series.Series:
                site_uri = URIRef(row["plot_uri"])
            else:
                site_uri = Site.generate_uri(ns, row[SITE_LOCATION_ID])
        except ValueError:
            # Column SITE_LOCATION_ID not available.
            pass

        if project_dataset_column and row[project_dataset_column]:
            project_or_dataset = project_dataset_column.split("_")
            dataset_uri = URIRef(
                f"{RDFDataset.generate_uri(ns).__str__()}/{project_or_dataset[0]}-{row[project_dataset_column]}"
            )
        else:
            dataset_uri = RDFDataset.generate_uri(ns)

        for column_name, column_mapping in mappings.items():
            # Notice that type of column_mapping["exclude"] is numpy.bool_ (not bool)
            # so using IS keyword won't work here
            if column_mapping["exclude"] is not None and column_mapping["exclude"] == False:
                phenomenom_time, result_time = get_observation_dates(
                    row, column_mapping, row[DEFAULT_DATETIME]
                )
                if column_mapping["record_type"] == RECORD_TYPE_OBSERVATION:
                    result, simple_result = get_result(
                        ns, vocabs, column_mapping, row, column_name, errors
                    )
                    if result:
                        sampler = None
                        sensor = None
                        if column_mapping["system_value"]:
                            try:
                                unique_id = row[INSTR_ATTR_UNIQUE_ID]
                            except ValueError as e:
                                unique_id = row[UNIQUE_ID]
                            except KeyError as e:
                                unique_id = row[UNIQUE_ID]
                            if column_mapping["system_type"]:
                                if column_mapping["system_type"] == "sensor":
                                    sensor = Sensor(
                                        uri=Sensor.generate_uri(
                                            ns,
                                            column_mapping["db_table"],
                                            column_mapping["db_column"],
                                            unique_id,
                                            False,
                                        ),
                                        system_type=URIRef(column_mapping["system_value"]),
                                    )
                                    g += sensor.g
                                elif column_mapping["system_type"] == "sampler":
                                    sampler = Sampler(
                                        uri=Sampler.generate_uri(
                                            ns,
                                            column_mapping["db_table"],
                                            column_mapping["db_column"],
                                            unique_id,
                                            False,
                                        ),
                                        system_type=URIRef(column_mapping["system_value"]),
                                        implements=URIRef(column_mapping["sample_method"]),
                                    )
                                    g += sampler.g
                                else:
                                    raise TypeError(
                                        f'Not recognised system_type value: {column_mapping["system_type"]}'
                                    )
                            else:
                                raise TypeError(
                                    f"Missing system_type value for row: {column_mapping}"
                                )
                        if column_mapping["_observation_of"] == "foi":
                            foi = FeatureOfInterest(
                                uri=FeatureOfInterest.generate_uri(
                                    ns, column_mapping["foi_columns"], row
                                ),
                                in_dataset=dataset_uri,
                                feature_type=URIRef(column_mapping["foi_type"]),
                                has_site=site_uri,
                                has_site_visit=site_visit_uri,
                            )
                            g += foi.g
                        elif column_mapping["_observation_of"] == "materialSample":
                            material_sample_id = FeatureOfInterest.generate_uri(
                                ns, column_mapping["foi_columns"], row
                            )
                            sampling = Sampling(
                                uri=Sampling.generate_uri(ns, row["sample_id"]),
                                in_dataset=dataset_uri,
                                has_feature_of_interest=site_uri,
                                used_procedure=URIRef(column_mapping["sample_method"]),
                                made_by_sampler=sampler,
                                has_result=material_sample_id,
                                has_site_visit=site_visit_uri,
                                result_datetime=Literal(result_time),
                            )
                            foi = MaterialSample(
                                uri=material_sample_id,
                                material_sample_id=material_sample_id,
                                in_dataset=dataset_uri,
                                feature_type=URIRef(column_mapping["foi_type"]),
                                is_sample_of=site_uri,
                                is_result_of=sampling,
                                identifier=Literal(row["sample_id"]),
                            )
                            g += foi.g
                        elif column_mapping["_observation_of"] == "sample":
                            sample_id = FeatureOfInterest.generate_uri(
                                ns, column_mapping["foi_columns"], row
                            )
                            sampling = Sampling(
                                uri=Sampling.generate_uri(ns, row["sample_id"]),
                                in_dataset=dataset_uri,
                                has_feature_of_interest=site_uri,
                                used_procedure=URIRef(column_mapping["sample_method"]),
                                made_by_sampler=sampler,
                                has_result=sample_id,
                                has_site_visit=site_visit_uri,
                                result_datetime=Literal(result_time),
                            )
                            foi = Sample(
                                uri=sample_id,
                                in_dataset=dataset_uri,
                                feature_type=URIRef(column_mapping["foi_type"]),
                                is_sample_of=site_uri,
                                is_result_of=sampling,
                                identifier=Literal(row["sample_id"]),
                            )
                            g += foi.g
                        else:
                            raise TypeError(
                                f'Not recognised _observation_of type: {column_mapping["_observation_of"]}'
                            )

                        # TODO: Every observation uses the class tern:Observation. If we require categorising,
                        #  create a controlled vocabulary for it like we do for features of interest.
                        # observation_cls = getattr(models, column_mapping["observation_type"])
                        tags = (
                            f"""{column_mapping["_parameter_name"]},{column_mapping["_foi_type"]}"""
                        )
                        if site_uri:
                            tags = f"""{tags},{str(row[SITE_LOCATION_ID])},{str(row[SITE_LOCATION_VISIT_ID])}"""
                        if column_mapping["_system_value"]:
                            tags += f""",{column_mapping["_system_value"]}"""
                        if column_mapping["tags"]:
                            tags += f""",{column_mapping["tags"]}"""

                        observation_cls = Observation
                        obs_uri = observation_cls.generate_uri(
                            ns,
                            column_mapping["db_table"],
                            column_mapping["db_column"],
                            row[UNIQUE_ID],
                        )
                        g += observation_cls(
                            uri=obs_uri,
                            feature_of_interest=foi,
                            in_dataset=dataset_uri,
                            observed_property=URIRef(column_mapping["parameter_global_value"]),
                            used_procedure=URIRef(column_mapping["method_value"]),
                            has_result=result,
                            has_simple_result=simple_result,
                            phenomenon_time=Instant(
                                uri=generate_underscore_uri(ns),
                                datetime=Literal(phenomenom_time, datatype=XSD.dateTime),
                            ),
                            result_datetime=Literal(result_time, datatype=XSD.dateTime),
                            made_by_sensor=sensor,
                            has_site=site_uri,
                            has_site_visit=site_visit_uri,
                            ecoplots_tags=Literal(tags, datatype=XSD.string),
                        ).g

                elif column_mapping["record_type"] == RECORD_TYPE_ATTRIBUTE:
                    result, simple_result = get_result(
                        ns, vocabs, column_mapping, row, column_name, errors
                    )
                    unique_id = row[UNIQUE_ID]
                    if result:
                        attribute_of_some_individuals = []
                        if column_mapping["_attribute_of"] == "site":
                            try:
                                unique_id = row[SITE_ATTR_UNIQUE_ID]
                            except ValueError as e:
                                pass
                            except KeyError as e:
                                pass
                            attribute_of_some_individuals.append(
                                Site.generate_uri(ns, row[SITE_LOCATION_ID])
                            )
                        elif column_mapping["_attribute_of"] == "siteVisit":
                            try:
                                unique_id = row[VISIT_ATTR_UNIQUE_ID]
                            except ValueError as e:
                                pass
                            except KeyError as e:
                                pass
                            attribute_of_some_individuals.append(
                                SiteVisit.generate_uri(ns, row[SITE_LOCATION_VISIT_ID])
                            )
                        elif column_mapping["_attribute_of"] == "system":
                            try:
                                unique_id = row[INSTR_ATTR_UNIQUE_ID]
                            except ValueError as e:
                                pass
                            except KeyError as e:
                                pass
                            if column_mapping["system_type"] == "sensor":
                                attribute_of_some_individuals.append(
                                    Sensor.generate_uri(
                                        ns,
                                        column_mapping["db_table"],
                                        column_mapping["observation_specific"],
                                        unique_id,
                                        False,
                                    )
                                )
                            elif column_mapping["system_type"] == "sampler":
                                attribute_of_some_individuals.append(
                                    Sampler.generate_uri(
                                        ns,
                                        column_mapping["db_table"],
                                        column_mapping["observation_specific"],
                                        unique_id,
                                        False,
                                    )
                                )
                            else:
                                raise TypeError(
                                    f'Not recognised system_type value: {column_mapping["system_type"]}'
                                )
                        elif column_mapping["_attribute_of"] == "foi":
                            try:
                                unique_id = row[FOI_ATTR_UNIQUE_ID]
                            except ValueError as e:
                                pass
                            except KeyError as e:
                                pass
                            foi_id = FeatureOfInterest.generate_uri(
                                ns, column_mapping["foi_columns"], row
                            )
                            # if column_mapping["system_type"] == "sampler":
                            #     sampling = Sampling(
                            #         uri=URIRef(f"{sampling}-{str(foi_id)}"),
                            #         in_dataset=dataset_uri,
                            #         has_result=foi_id,
                            #         has_feature_of_interest=site_uri,
                            #         used_procedure=URIRef(column_mapping["method_value"]),
                            #         has_site=site_uri,
                            #         has_site_visit=site_visit_uri,
                            #         result_datetime=Literal(result_time),
                            #     )
                            # We create the Feature of Interest here.
                            attribute_of_some_individuals.append(
                                FeatureOfInterest(
                                    uri=foi_id,
                                    in_dataset=dataset_uri,
                                    feature_type=URIRef(column_mapping["foi_type"]),
                                    has_site=site_uri,
                                    has_site_visit=site_visit_uri,
                                )
                            )
                            for ind in attribute_of_some_individuals:
                                g += ind.g
                        elif column_mapping["_attribute_of"] == "materialSample":
                            try:
                                unique_id = row[MS_ATTR_UNIQUE_ID]
                            except ValueError as e:
                                pass
                            except KeyError as e:
                                pass
                            material_sample_id = FeatureOfInterest.generate_uri(
                                ns, column_mapping["foi_columns"], row
                            )
                            sampling = Sampling(
                                uri=Sampling.generate_uri(ns, row["sample_id"]),
                                in_dataset=dataset_uri,
                                has_feature_of_interest=site_uri,
                                used_procedure=URIRef(column_mapping["sample_method"]),
                                has_result=material_sample_id,
                                has_site=site_uri,
                                has_site_visit=site_visit_uri,
                                result_datetime=Literal(result_time),
                            )
                            attribute_of_some_individuals.append(
                                MaterialSample(
                                    uri=material_sample_id,
                                    material_sample_id=material_sample_id,
                                    in_dataset=dataset_uri,
                                    feature_type=URIRef(column_mapping["foi_type"]),
                                    is_sample_of=site_uri,
                                    is_result_of=sampling,
                                    identifier=Literal(row["sample_id"]),
                                )
                            )
                            for ind in attribute_of_some_individuals:
                                g += ind.g
                        elif column_mapping["_attribute_of"] == "sample":
                            try:
                                unique_id = row[SAMPLE_ATTR_UNIQUE_ID]
                            except ValueError as e:
                                pass
                            except KeyError as e:
                                pass
                            sample_id = FeatureOfInterest.generate_uri(
                                ns, column_mapping["foi_columns"], row
                            )
                            sampling = Sampling(
                                uri=Sampling.generate_uri(ns, row["sample_id"]),
                                in_dataset=dataset_uri,
                                has_result=sample_id,
                                has_feature_of_interest=site_uri,
                                used_procedure=URIRef(column_mapping["sample_method"]),
                                has_site=site_uri,
                                has_site_visit=site_visit_uri,
                                result_datetime=Literal(result_time),
                            )
                            attribute_of_some_individuals.append(
                                Sample(
                                    uri=sample_id,
                                    in_dataset=dataset_uri,
                                    feature_type=URIRef(column_mapping["foi_type"]),
                                    is_sample_of=site_uri,
                                    is_result_of=sampling,
                                    identifier=Literal(row["sample_id"]),
                                )
                            )
                            for ind in attribute_of_some_individuals:
                                g += ind.g
                        elif column_mapping["_attribute_of"] == "sampling":
                            attribute_of_some_individuals.append(
                                Sampling.generate_uri(ns, row["sample_id"])
                            )
                        elif column_mapping["_attribute_of"] == "observation":
                            attribute_of_some_individuals = get_on_observation_uris(
                                ns,
                                column_mapping["db_table"],
                                column_mapping["observation_specific"],
                                unique_id,
                            )
                        else:
                            raise TypeError(
                                f'Not recognised _attribute_of type: {column_mapping["_attribute_of"]}'
                            )
                        new_attribute_uri = Attribute.generate_uri(
                            ns,
                            column_mapping["db_table"],
                            column_mapping["db_column"],
                            unique_id,
                        )
                        if not (new_attribute_uri, RDF.type, TERN.Attribute) in g:
                            for ind in attribute_of_some_individuals:
                                g += Attribute(
                                    uri=new_attribute_uri,
                                    is_attribute_of=ind,
                                    in_dataset=dataset_uri,
                                    attribute=URIRef(column_mapping["attribute_value"]),
                                    has_value=result,
                                    has_simple_value=simple_result,
                                ).g
                            if (
                                column_mapping["lut_table"] is not None
                                and column_mapping["lut_table"] != ""
                            ):
                                g.add(
                                    (
                                        URIRef(column_mapping["attribute_value"]),
                                        DCTERMS.relation,
                                        URIRef(column_mapping["lut_table"]),
                                    )
                                )
                else:
                    if type(rows) == pd.core.frame.DataFrame:
                        errors.append(
                            "'record_type' not in (Observation, Attribute) for column={}".format(
                                column_name
                            )
                        )
                    else:
                        add_error(
                            errors,
                            "'record_type' not in (Observation, Attribute) for column={}".format(
                                column_name
                            ),
                        )

    return g


def get_on_observation_system_uris(ns, table, observation_specific, unique_id):
    if observation_specific:
        uris_list = []
        observations_list = observation_specific.split(",")
        for obs in observations_list:
            uris_list.append(
                Sensor.generate_uri(
                    ns,
                    table,
                    obs,
                    unique_id,
                )
            )
        return uris_list
    else:
        return None


def get_on_observation_uris(ns, table, observation_specific, unique_id):
    if observation_specific:
        uris_list = []
        observations_list = observation_specific.split(",")
        for obs in observations_list:
            uris_list.append(
                Observation.generate_uri(
                    ns,
                    table,
                    obs,
                    unique_id,
                )
            )
        return uris_list
    else:
        return None


#
# def get_result_doc(attr, vocabs, column_mapping, yb_mappings, row, column_name, errors, matched_species):
#     label = None
#     uom = None
#     taxon = None
#     if row[column_name] is not None:
#         if column_mapping["result_type"] == "String":
#             if row[column_name] == "":
#                 return None, None
#             else:
#                 type = "string"
#
#         elif column_mapping["result_type"] == "Number":
#             type = "float"
#             uom = column_mapping["result_uom"]
#         elif column_mapping["result_type"] == "Boolean":
#             type = "boolean"
#         elif column_mapping["result_type"] == "Date":
#             type = "date"
#         elif column_mapping["result_type"] == "DateTime":
#             type = "datetime"
#         elif column_mapping["result_type"] == "IRI":
#             type = "uri"
#         elif column_mapping["result_type"] == "Taxon":
#             species = matched_species.get(row[column_name])
#             if species:
#                 taxon = {
#                     "label": species["acceptedNameUsage"],
#                     "taxon_value": {
#                         "taxon_accepted_name_label": species["acceptedNameUsage"],
#                         "taxon_accepted_name": species["acceptedNameUsageID"],
#                         "taxon_higher_classification": species["higherClassification"],
#                         "taxon_class": species["class_"],
#                         "taxon_family": species["family"],
#                         "taxon_kingdom": species["kingdom"],
#                         "taxon_name_according_to_label": species["nameAccordingTo"],
#                         "taxon_name_according_to": species["nameAccordingToID"],
#                         "taxon_nomenclatural_code": species["nomenclaturalCode"],
#                         "taxon_nomenclatural_status": species["nomenclaturalStatus"],
#                         "taxon_scientific_name": species["scientificNameID"],
#                         "taxon_scientific_name_authorship": species["scientificNameAuthorship"],
#                         "taxon_scientific_name_label": species["scientificName"],
#                         "taxon_concept_id": species["taxonConceptID"],
#                         "taxon_taxon_id": species["taxonID"],
#                         "taxon_taxonomic_status": species["taxonomicStatus"],
#                     },
#                     "type": "taxon",
#                 }
#                 return taxon, None
#             else:
#                 return None, None
#         elif column_mapping["result_type"] == "Concept":
#             type = "uri"
#             result_uri, label = get_vocabulary_concept_label(vocabs, URIRef(column_mapping["lut_table"]),
#                                                              URIRef(column_mapping["lut_predicate"]), row[column_name])
#             if not result_uri:
#                 errors.append('Concept was not found for {}: {}'.format(column_name, row[column_name]))
#                 return None, None
#         else:
#             errors.append("result_type not in (String, Number, Boolean, Date, DateTime, Concept) for column={}".format(
#                 column_name))
#             return None, None
#
#         if attr:
#             return {
#                        "label": label,
#                        f"value_{type}": row[column_name],
#                        "type": type
#                    }, uom
#         else:
#             return {
#                        "label": label,
#                        "value": taxon if taxon else row[column_name],
#                        "type": type
#                    }, uom
#
#     else:
#         return None, None


def get_result(ns, vocabs, column_mapping, row, column_name, errors):
    if type(row) == pd.core.series.Series:  # For Pandas
        if column_name in row.index:
            if row[column_name] is not None:
                return get_result_by_type(ns, vocabs, column_mapping, row, column_name, errors)
            else:
                return None, None
        else:
            return None, None
    elif row[column_name] is not None:  # For Spark
        return get_result_by_type(ns, vocabs, column_mapping, row, column_name, errors)
    else:
        return None, None


def get_result_by_type(ns, vocabs, column_mapping, row, column_name, errors):
    if column_mapping["result_type"] == "String":
        if row[column_name] == "":
            return None, None
        else:
            result = Text(
                uri=generate_underscore_uri(ns),
                value=Literal(row[column_name], datatype=XSD.string),
            )
            return result, result.value
    elif column_mapping["result_type"] == "Float":
        result = Float(
            generate_underscore_uri(ns),
            Literal(row[column_name], datatype=XSD.decimal),
            URIRef(column_mapping["result_uom"]),
        )
        return result, result.value
    elif column_mapping["result_type"] == "Integer":
        result = Integer(
            generate_underscore_uri(ns),
            Literal(row[column_name], datatype=XSD.integer),
            URIRef(column_mapping["result_uom"]),
        )
        return result, result.value
    elif column_mapping["result_type"] == "Boolean":
        result = Boolean(
            uri=generate_underscore_uri(ns), value=Literal(row[column_name], datatype=XSD.boolean)
        )
        return result, result.value
    elif column_mapping["result_type"] == "Date":
        result = Date(
            uri=generate_underscore_uri(ns), value=Literal(row[column_name], datatype=XSD.date)
        )
        return result, result.value
    elif column_mapping["result_type"] == "DateTime":
        result = DateTime(
            uri=generate_underscore_uri(ns), value=Literal(row[column_name], datatype=XSD.dateTime)
        )
        return result, result.value
    elif column_mapping["result_type"] == "IRI":
        # Concept: look for vocabs
        if column_mapping["lut_table"]:
            result_uri = get_vocabulary_concept(
                vocabs,
                URIRef(column_mapping["lut_table"]),
                URIRef(column_mapping["lut_predicate"]),
                row[column_name],
            )
            if result_uri:
                result = IRI(uri=generate_underscore_uri(ns), value=URIRef(result_uri))
            else:
                if type(errors) == list:
                    errors.append(
                        "Concept (IRI) was not found for {}: {}".format(
                            column_name, row[column_name]
                        )
                    )
                else:
                    add_error(
                        errors,
                        "Concept (IRI) was not found for {}: {}".format(
                            column_name, row[column_name]
                        ),
                    )
                return None, None
        # Simple IRI (also former Concept type)
        else:
            result = IRI(uri=generate_underscore_uri(ns), value=URIRef(row[column_name]))
        return result, result.value
    elif column_mapping["result_type"] == "Taxon":
        result = Taxon(
            uri=generate_underscore_uri(ns),
            label=Literal(row[column_name]),
            taxon_id=generate_underscore_uri(ns),
        )
        return result, Literal(row[column_name], datatype=XSD.string)
    else:
        if type(errors) == list:
            errors.append(
                f"result_type:'{column_mapping['result_type']}' not in (String, Float, Integer, Boolean, Date, DateTime, IRI) for column={column_name}"
            )
        else:
            add_error(
                errors,
                f"result_type:'{column_mapping['result_type']}' not in (String, Float, Integer, Boolean, Date, DateTime, IRI) for column={column_name}",
            )
        return None, None
