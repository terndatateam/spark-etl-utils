from rdflib import Graph, RDF, RDFS


def infer_subclasses(g: Graph, ontology: Graph, class_name: None):
    """
    infer_subclasses
​
    if:
        T(?c1, rdfs:subClassOf, ?c2)
        T(?x, rdf:type, ?c1)
    then:
        T(?x, rdf:type, ?c2)
    """
    for c1, _, c2 in ontology.triples((None, RDFS.subClassOf, class_name)):
        infer_subclasses(g, ontology, c1)
        for x, _, _ in g.triples((None, RDF.type, c1)):
            g.add((x, RDF.type, c2))
