import os
from typing import Dict

import pandas
import requests
from pyspark import Accumulator

from spark_etl_utils import add_error
from spark_etl_utils.database import get_postgres_url

DESTINATION_PATH = "vocabulary_mappings.csv"


def upload_vocabulary_mappings(spark, csv_url, dataset, table, mode):
    db_host = os.getenv('POSTGRES_HOST')
    db_port = os.getenv('POSTGRES_PORT')
    db_name = os.getenv('POSTGRES_DB')
    db_username = os.getenv('POSTGRES_USER')
    db_password = os.getenv('POSTGRES_PASSWORD')
    db_schema = dataset

    print("Downloading CSV vocabulary mappings from: '{}'.".format(csv_url))

    req = requests.get(csv_url)
    url_content = req.content
    csv_file = open(DESTINATION_PATH, 'wb+')
    csv_file.write(url_content)
    csv_file.close()

    df = spark.read.load(DESTINATION_PATH, format="csv", sep=",", inferSchema="true", header="true")

    print("Writting CSV into postgres, database: '{}', table: '{}'.".format(db_name, "{}.{}".format(db_schema, table)))

    df.write.jdbc(get_postgres_url(db_host, db_port, db_name), "{}.{}".format(db_schema, table),
                  mode=mode, properties={"user": db_username, "password": db_password})

    print("Vocabulary mapping uploaded with success.")


def get_mappings(vocabulary_mappings: pandas.DataFrame, errors: Accumulator) -> Dict:
    """
    Get all the mappings from the DataFrame for all the columns in the source table and return it as Dictionary
    :param vocabulary_mappings: Pandas dataFrame containing the "Mappings spreadsheet"
    :param errors: Spark accumulator for storing errors during execution
    :return
    """
    columns = vocabulary_mappings["db_column"].to_list()
    mappings = {}
    for col in columns:
        col_mapping = get_mappings_column(vocabulary_mappings, col, errors)
        if col_mapping:
            mappings.update({col: col_mapping})
    return mappings


def get_mappings_es(vocabulary_mappings: pandas.DataFrame, table_name, errors: Accumulator) -> Dict:
    """
    Get all the mappings from the DataFrame for all the columns in the source table and return it as Dictionary
    :param vocabulary_mappings: Pandas dataFrame containing the "Mappings spreadsheet"
    :param errors: Spark accumulator for storing errors during execution
    :return
    """
    columns = vocabulary_mappings.query("db_table == '{}'".format(table_name))["db_column"].to_list()
    mappings = {}
    for col in columns:
        col_mapping = get_mappings_column_es(vocabulary_mappings, table_name, col, errors)
        if col_mapping:
            mappings.update({col: col_mapping})
    return mappings


# def get_yb_mappings(yb_vocabulary_mappings: pandas.DataFrame, errors: Accumulator) -> Dict:
#     """
#     Get all the mappings from the DataFrame for all the columns in the source table and return it as Dictionary
#     :param vocabulary_mappings: Pandas dataFrame containing the "Mappings spreadsheet"
#     :param errors: Spark accumulator for storing errors during execution
#     :return
#     """
#     columns = yb_vocabulary_mappings["db_column"].to_list()
#     mappings = {}
#     for col in columns:
#         col_mapping = get_yb_mappings_column(yb_vocabulary_mappings, col, errors)
#         if col_mapping:
#             mappings.update({col: col_mapping})
#     return mappings


def get_mappings_column(vocabulary_mappings: pandas.DataFrame, db_column: str, errors: Accumulator) -> Dict:
    """
    Get all the mappings from the DataFrame for a specific "db_column" and return it as Dictionary
    :param vocabulary_mappings: Pandas dataFrame containing the "Mappings spreadsheet"
    :param db_column: Filter by this value in db_column table in the "mapping spreadsheet"
    :param errors: Spark accumulator for storing errors during execution
    :return
    """
    parameter_info = vocabulary_mappings.query("db_column == '{}'".format(db_column))
    if parameter_info.empty:
        add_error(errors, "Did not find mappings for db_column={}".format(db_column))
        return {}
    else:
        param_info = {}
        for column in vocabulary_mappings.columns:
            param_info.update({column: parameter_info.iloc[0][column]})
        return param_info


def get_mappings_column_es(vocabulary_mappings: pandas.DataFrame, table_name, db_column: str,
                           errors: Accumulator) -> Dict:
    """
    Get all the mappings from the DataFrame for a specific "db_column" and return it as Dictionary
    :param vocabulary_mappings: Pandas dataFrame containing the "Mappings spreadsheet"
    :param db_column: Filter by this value in db_column table in the "mapping spreadsheet"
    :param errors: Spark accumulator for storing errors during execution
    :return
    """
    parameter_info = vocabulary_mappings.query("db_table == '{}' and db_column == '{}'".format(table_name, db_column))
    if parameter_info.empty:
        add_error(errors, "Did not find mappings for db_column={}".format(db_column))
        return {}
    else:
        param_info = {}
        for column in vocabulary_mappings.columns:
            param_info.update({column: parameter_info.iloc[0][column]})
        return param_info


def get_yb_mappings_column(yb_mappings: pandas.DataFrame, value: str, errors: Accumulator):
    yb_mapping = yb_mappings.query("dataset_concept_uri == '{}'".format(value))
    if yb_mapping.empty:
        # add_error(errors, "Did not find master mapping for concept_uri={}".format(value))
        return {}
    else:
        param_info = {}
        for column in yb_mappings.columns:
            param_info.update({column: yb_mapping.iloc[0][column]})
        return param_info
