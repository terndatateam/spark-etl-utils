from abc import ABC
from typing import Union, Optional, List
from uuid import uuid4

from pyspark.sql.types import Row
from rdf_orm import RDFModel, MapTo
from rdflib import Namespace, URIRef, Literal, BNode
from rdflib.namespace import RDF, RDFS, TIME
from tern_rdf.namespace_bindings import (
    ADMS,
    DCAT,
    TERN,
    SOSA,
    DCTERMS,
    VOID,
    PROV,
    GEOSPARQL,
    TERN_LOC,
    W3CGEO,
    DWCT,
    SKOS,
    SSN,
    SCHEMA,
)


def generate_underscore_uri(namespace: Namespace):
    return namespace["_" + str(uuid4())]


class RDFDataset(RDFModel):
    class_uri = TERN.RDFDataset
    dcterms_type = TERN.RDFDataset

    mapping = {
        "dcterms_type": DCTERMS.type,
        "abstract": DCTERMS.abstract,
        "title": DCTERMS.title,
        "alternative": DCTERMS.alternative,
        "description": DCTERMS.description,
        "creator": DCTERMS.creator,
        "created": DCTERMS.created,
        "publisher": DCTERMS.publisher,
        "issued": DCTERMS.issued,
        "citation": DCTERMS.bibliographicCitation,
        "subset": VOID.subset,
        "dataset_type": URIRef("urn:ecoplots:dataset_type"),
    }

    title: Optional[Literal] = None
    abstract: Optional[Literal] = None
    description: Optional[Literal] = None
    issued: Optional[Literal] = None
    alternative: Optional[Literal] = None
    creator: Optional[Literal] = None
    publisher: Optional[Literal] = None
    citation: Optional[Literal] = None
    subset: Optional[Literal] = None
    created: Optional[Literal] = None
    dataset_type: Optional[Literal] = None

    def __init__(
        self,
        uri: URIRef,
        title: Optional[Literal] = None,
        abstract: Optional[Literal] = None,
        description: Optional[Literal] = None,
        issued: Optional[Literal] = None,
        creator: Optional[Literal] = None,
        publisher: Optional[Literal] = None,
        citation: Optional[Literal] = None,
        alternative: Optional[Literal] = None,
        subset: Optional[Literal] = None,
        created: Optional[Literal] = None,
        dataset_type: Optional[Literal] = None,
    ):
        super(RDFDataset, self).__init__()
        self.assign_constructor_vars(locals())

    @staticmethod
    def generate_uri(namespace: Namespace) -> URIRef:
        if namespace.__str__()[-1:] == "/":
            return URIRef(namespace.__str__()[:-1])
        else:
            return URIRef(namespace)


class System(RDFModel):
    class_uri = TERN.System
    dcterms_type = TERN.System

    mapping = {
        "dcterms_type": DCTERMS.type,
        "system_type": TERN.systemType,
        "has_deployment": SSN.hasDeployment,
        "implements": SSN.implements,
        "is_hosted_by": SOSA.isHostedBy,
    }

    system_type: URIRef
    attribute: Optional[List[URIRef]] = None
    has_deployment: Optional[URIRef] = None
    is_hosted_by: Optional[URIRef] = None
    implements: Optional[URIRef] = None

    def __init__(
        self,
        uri: URIRef,
        system_type: URIRef,
        has_deployment: Optional[URIRef] = None,
        is_hosted_by: Optional[URIRef] = None,
        implements: Optional[URIRef] = None,
    ):
        super(System, self).__init__()
        self.assign_constructor_vars(locals())


class Sensor(RDFModel):
    class_uri = TERN.Sensor
    dcterms_type = TERN.Sensor

    mapping = {
        "dcterms_type": DCTERMS.type,
        "system_type": TERN.systemType,
        "has_deployment": SSN.hasDeployment,
        "implements": SSN.implements,
        "is_hosted_by": SOSA.isHostedBy,
        "made_observation": SOSA.madeObservation,
        "observes": SOSA.observes,
    }

    system_type: URIRef
    has_deployment: Optional[URIRef] = None
    is_hosted_by: Optional[URIRef] = None
    implements: Optional[URIRef] = None
    made_observation: Optional[List[URIRef]] = None
    observes: Optional[URIRef] = None

    def __init__(
        self,
        uri: URIRef,
        system_type: URIRef,
        has_deployment: Optional[URIRef] = None,
        is_hosted_by: Optional[URIRef] = None,
        implements: Optional[URIRef] = None,
        made_observation: Optional[List[URIRef]] = None,
        observes: Optional[URIRef] = None,
    ):
        super(Sensor, self).__init__()
        self.assign_constructor_vars(locals())

    @staticmethod
    def generate_uri(
        namespace: Namespace,
        table_name: str,
        parameter_name: str,
        unique_id: Union[str, int],
        is_bnode=False,
    ) -> Union[URIRef, BNode]:
        uri = "{table_name}-sensor-{parameter_name}-{unique_id}".format(
            table_name=table_name,
            parameter_name=parameter_name,
            unique_id=unique_id,
        ).lower()
        if is_bnode:
            return BNode(namespace[uri])
        else:
            return URIRef(namespace[uri])


class Sampler(RDFModel):
    class_uri = TERN.Sampler
    dcterms_type = TERN.Sampler

    mapping = {
        "dcterms_type": DCTERMS.type,
        "system_type": TERN.systemType,
        "has_deployment": SSN.hasDeployment,
        "implements": SSN.implements,
        "is_hosted_by": SOSA.isHostedBy,
        "made_sampling": SOSA.madeSampling,
    }

    system_type: URIRef
    has_deployment: Optional[URIRef] = None
    is_hosted_by: Optional[URIRef] = None
    implements: Optional[URIRef] = None
    made_sampling: Optional[List[URIRef]] = None

    def __init__(
        self,
        uri: URIRef,
        system_type: URIRef,
        has_deployment: Optional[URIRef] = None,
        is_hosted_by: Optional[URIRef] = None,
        implements: Optional[URIRef] = None,
        made_sampling: Optional[List[URIRef]] = None,
    ):
        super(Sampler, self).__init__()
        self.assign_constructor_vars(locals())

    @staticmethod
    def generate_uri(
        namespace: Namespace,
        table_name: str,
        parameter_name: str,
        unique_id: Union[str, int],
        is_bnode=False,
    ) -> Union[URIRef, BNode]:
        uri = "{table_name}-sampler-{parameter_name}-{unique_id}".format(
            table_name=table_name,
            parameter_name=parameter_name,
            unique_id=unique_id,
        ).lower()
        if is_bnode:
            return BNode(namespace[uri])
        else:
            return URIRef(namespace[uri])


class FeatureOfInterest(RDFModel):
    class_uri = TERN.FeatureOfInterest
    dcterms_type = TERN.FeatureOfInterest

    mapping = {
        "dcterms_type": DCTERMS.type,
        "identifier": DCTERMS.identifier,
        "in_dataset": VOID.inDataset,
        "has_geometry": GEOSPARQL.hasGeometry,
        "is_feature_of_interest_of": SOSA.isFeatureOfInterestOf,
        "comment": RDFS.comment,
        "feature_type": TERN.featureType,
        "has_sample": SOSA.hasSample,
        "has_attribute": TERN.Attribute,
        "has_site": MapTo(TERN.hasSite, TERN.isSiteOf),
        "has_site_visit": MapTo(TERN.hasSiteVisit, TERN.isSiteVisitOf),
    }

    in_dataset: URIRef
    feature_type: URIRef
    identifier: Optional[Literal] = None
    has_site: Optional[Union[URIRef, "Site"]] = None
    has_site_visit: Optional[Union[URIRef, "SiteVisit"]] = None
    is_feature_of_interest_of: Optional[URIRef] = None
    has_sample: Optional[List[URIRef]] = None
    has_attribute: Optional[List[URIRef]] = None
    has_geometry: Optional[URIRef] = None
    comment: Optional[Literal] = None

    def __init__(
        self,
        uri: URIRef,
        in_dataset: URIRef,
        feature_type: URIRef,
        identifier: Optional[Literal] = None,
        has_site: Optional[Union[URIRef, "Site"]] = None,
        has_site_visit: Optional[Union[URIRef, "SiteVisit"]] = None,
        is_feature_of_interest_of: Optional[URIRef] = None,
        has_sample: Optional[List[URIRef]] = None,
        has_attribute: Optional[List[URIRef]] = None,
        has_geometry: Optional[URIRef] = None,
        comment: Optional[Literal] = None,
    ):
        super(FeatureOfInterest, self).__init__()
        self.assign_constructor_vars(locals())

    @staticmethod
    def generate_uri(namespace: Namespace, foi_columns: str, row: Row) -> Optional[URIRef]:
        if not foi_columns:
            return None
        foi_columns_split = foi_columns.split(",")
        unique_id = ""
        for column in foi_columns_split:
            unique_id += "{col}-{value}-".format(col=column, value=row[column])
        return URIRef(namespace[unique_id[:-1].lower()])


class Site(RDFModel):
    class_uri = TERN.Site
    dcterms_type = TERN.Site

    mapping = {
        "dcterms_type": DCTERMS.type,
        "site_description": TERN.siteDescription,
        "dimension": TERN.dimension,
        "has_site_visit": MapTo(TERN.hasSiteVisit, TERN.hasSite),
        "sf_within": GEOSPARQL.sfWithin,
        "location_description": TERN.locationDescription,
        "location_procedure": TERN.locationProcedure,
        "in_dataset": VOID.inDataset,
        "feature_type": TERN.featureType,
        "identifier": DCTERMS.identifier,
        "has_geometry": GEOSPARQL.hasGeometry,
        "comment": RDFS.comment,
        "qualified_association": PROV.qualifiedAssociation,
        "was_attributed_to": PROV.wasAttributedTo,
        "has_attribute": TERN.hasAttribute,
        "has_sample": SOSA.hasSample,
        "is_sample_of": SOSA.isSampleOf,
        "date_commissioned": TERN.dateCommissioned,
        "date_decommissioned": TERN.dateDecommissioned,
    }

    in_dataset: URIRef
    feature_type: URIRef
    date_commissioned: Optional[Literal] = None
    has_site_visit: Optional[URIRef] = None
    label: Optional[Literal] = None
    identifier: Optional[Literal] = None
    has_sample: Optional[URIRef] = None
    is_sample_of: Optional[URIRef] = None
    date_decommissioned: Optional[Literal] = None
    dimension: Optional[Literal] = None
    location_description: Optional[Literal] = None
    location_procedure: Optional[URIRef] = None
    polygon: Optional[URIRef] = None
    site_description: Optional[Literal] = None
    has_attribute: Optional[List[URIRef]] = None
    has_geometry: Optional[URIRef] = None
    sf_within: Optional[URIRef] = None
    comment: Optional[Literal] = None

    qualified_association: Optional[URIRef] = None
    was_attributed_to: Optional[URIRef] = None

    def __init__(
        self,
        uri: URIRef,
        in_dataset: URIRef,
        feature_type: URIRef,
        date_commissioned: Optional[Literal] = None,
        has_site_visit: Optional[URIRef] = None,
        identifier: Optional[Literal] = None,
        label: Optional[Literal] = None,
        has_sample: Optional[URIRef] = None,
        is_sample_of: Optional[URIRef] = None,
        date_decommissioned: Optional[Literal] = None,
        dimension: Optional[Literal] = None,
        location_description: Optional[Literal] = None,
        location_procedure: Optional[URIRef] = None,
        polygon: Optional[URIRef] = None,
        site_description: Optional[Literal] = None,
        has_attribute: Optional[List[URIRef]] = None,
        has_geometry: Optional[URIRef] = None,
        sf_within: Optional[URIRef] = None,
        qualified_association: Optional[URIRef] = None,
        was_attributed_to: Optional[URIRef] = None,
        comment: Optional[Literal] = None,
    ):
        super(Site, self).__init__()
        self.assign_constructor_vars(locals())

    @staticmethod
    def generate_uri(namespace: Namespace, site_id: Union[str, int]) -> URIRef:
        return URIRef(namespace["site-{}".format(site_id).lower()])


class Transect(Site):
    class_uri = TERN.Transect
    dcterms_type = TERN.Transect

    mapping = {
        "dcterms_type": DCTERMS.type,
        "label": RDFS.label,
        "has_geometry": GEOSPARQL.hasGeometry,
        "has_attribute": TERN.hasAttribute,
        "identifier": DCTERMS.identifier,
        "in_dataset": VOID.inDataset,
        "has_site_visit": MapTo(TERN.hasSiteVisit, TERN.hasSite),
        "feature_type": TERN.featureType,
        "transect_direction": TERN.transectDirection,
        "transect_start_point": TERN.transectStartPoint,
        "transect_end_point": TERN.transectEndPoint,
        "has_sample": SOSA.hasSample,
        "is_sample_of": SOSA.isSampleOf,
    }

    transect_direction: Optional[Union[URIRef, Literal]] = None
    transect_start_point: Optional[URIRef] = None
    transect_end_point: Optional[URIRef] = None

    def __init__(
        self,
        uri: URIRef,
        in_dataset: URIRef,
        feature_type: URIRef,
        has_site_visit: Optional[URIRef] = None,
        identifier: Optional[Literal] = None,
        has_attribute: Optional[List[URIRef]] = None,
        has_geometry: Optional[URIRef] = None,
        transect_direction: Optional[Union[URIRef, Literal]] = None,
        transect_start_point: Optional[URIRef] = None,
        transect_end_point: Optional[URIRef] = None,
        has_sample: Optional[URIRef] = None,
        is_sample_of: Optional[URIRef] = None,
    ):
        expected_feature_type = (
            "http://linked.data.gov.au/def/tern-cv/de46fa49-d1c9-4bef-8462-d7ee5174e1e1"
        )
        if str(feature_type) != expected_feature_type:
            raise TypeError(
                f"An instance of class Transect must have feature_type as {expected_feature_type}"
            )
        self.assign_constructor_vars(locals())


class AusPlotsSite(Site):
    class_uri = TERN.AusPlotsSite
    dcterms_type = TERN.AusPlotsSite


class AusPlotsRangelandsSite(AusPlotsSite):
    class_uri = TERN.AusPlotsRangelandsSite
    dcterms_type = TERN.AusPlotsRangelandsSite


class AusPlotsForestsSite(AusPlotsSite):
    class_uri = TERN.AusPlotsForestsSite
    dcterms_type = TERN.AusPlotsForestsSite


class CORVEGSite(Site):
    class_uri = TERN.CORVEGSite
    dcterms_type = TERN.CORVEGSite


class SiteVisit(RDFModel):
    class_uri = TERN.SiteVisit
    dcterms_type = TERN.SiteVisit

    mapping = {
        "dcterms_type": DCTERMS.type,
        "in_dataset": VOID.inDataset,
        "identifier": DCTERMS.identifier,
        "qualified_association": PROV.qualifiedAssociation,
        "was_associated_with": PROV.wasAssociatedWith,
        "location_description": TERN.locationDescription,
        "site_description": TERN.siteDescription,
        "started_at_time": PROV.startedAtTime,
        "ended_at_time": PROV.endedAtTime,
        "has_attribute": TERN.hasAttribute,
        "has_observation": TERN.hasObservation,
        "has_sampling": TERN.hasSampling,
        "has_site": MapTo(TERN.hasSite, TERN.hasSiteVisit),
        "has_sample": SOSA.hasSample,
        "is_sample_of": SOSA.isSampleOf,
    }

    in_dataset: URIRef
    started_at_time: Literal
    ended_at_time: Optional[Literal] = None
    has_site: Optional[URIRef] = None
    identifier: Optional[Literal] = None
    location_description: Optional[Literal] = None
    site_description: Optional[Literal] = None
    has_attribute: Optional[List[URIRef]] = None
    has_observation: Optional[List[URIRef]] = None
    has_sampling: Optional[List[URIRef]] = None
    has_sample: Optional[URIRef] = None
    is_sample_of: Optional[URIRef] = None

    qualified_association: Optional[URIRef] = None
    was_associated_with: Optional[URIRef] = None

    def __init__(
        self,
        uri: URIRef,
        in_dataset: URIRef,
        started_at_time: Literal,
        ended_at_time: Optional[Literal] = None,
        has_site: Optional[URIRef] = None,
        identifier: Optional[Literal] = None,
        location_description: Optional[Literal] = None,
        site_description: Optional[Literal] = None,
        has_attribute: Optional[List[URIRef]] = None,
        has_observation: Optional[List[URIRef]] = None,
        has_sampling: Optional[List[URIRef]] = None,
        has_sample: Optional[URIRef] = None,
        is_sample_of: Optional[URIRef] = None,
        qualified_association: Optional[URIRef] = None,
        was_associated_with: Optional[URIRef] = None,
    ):
        super(SiteVisit, self).__init__()
        self.assign_constructor_vars(locals())

    @staticmethod
    def generate_uri(namespace: Namespace, site_visit_id: Union[str, int]) -> URIRef:
        return URIRef(namespace["sv-{}".format(site_visit_id).lower()])


class ObservableProperty(RDFModel):
    class_uri = TERN.ObservableProperty

    mapping = {
        "value": RDF.value,
        "vocabulary": TERN.vocabulary,
        "local_value": TERN.localValue,
        "local_vocabulary": TERN.localVocabulary,
    }

    value: URIRef
    vocabulary: URIRef
    local_value: Optional[URIRef] = None
    local_vocabulary: Optional[URIRef] = None

    def __init__(
        self,
        uri: URIRef,
        value: URIRef,
        vocabulary: URIRef,
        local_value: Optional[URIRef] = None,
        local_vocabulary: Optional[URIRef] = None,
    ):
        super(ObservableProperty, self).__init__()
        self.assign_constructor_vars(locals())


class Value(ABC, RDFModel):
    class_uri = TERN.Value


class Result(Value):
    class_uri = TERN.Result


class Sampling(RDFModel):
    class_uri = TERN.Sampling
    dcterms_type = TERN.Sampling

    mapping = {
        "dcterms_type": DCTERMS.type,
        "in_dataset": VOID.inDataset,
        "identifier": DCTERMS.identifier,
        "has_geometry": GEOSPARQL.hasGeometry,
        "comment": RDFS.comment,
        "qualified_association": PROV.qualifiedAssociation,
        "was_associated_with": PROV.wasAssociatedWith,
        "has_feature_of_interest": SOSA.hasFeatureOfInterest,
        "result_datetime": TERN.resultDateTime,
        "used_procedure": SOSA.usedProcedure,
        "has_site_visit": TERN.hasSiteVisit,
        "has_site": TERN.hasSite,
        "has_result": MapTo(SOSA.hasResult, SOSA.isResultOf),
        "made_by_sampler": MapTo(SOSA.madeBySampler, SOSA.madeSampling),
        "sampling_type": TERN.samplingType,
    }

    in_dataset: Union[RDFDataset, URIRef]
    result_datetime: Literal
    used_procedure: URIRef
    has_result: URIRef
    identifier: Optional[Literal] = None
    has_geometry: Optional[URIRef] = None
    comment: Optional[Literal] = None
    has_feature_of_interest: Union[FeatureOfInterest, URIRef] = None
    qualified_association: Optional[URIRef] = None
    was_associated_with: Optional[URIRef] = None
    has_site_visit: Union[SiteVisit, URIRef] = None
    has_site: Union[Site, URIRef] = None
    made_by_sampler: Optional[URIRef] = None
    sampling_type: Optional[URIRef] = None

    def __init__(
        self,
        uri: URIRef,
        in_dataset: Union[RDFDataset, URIRef],
        has_feature_of_interest: Union[FeatureOfInterest, URIRef],
        result_datetime: Literal,
        used_procedure: URIRef,
        has_result: URIRef,
        identifier: Optional[Literal] = None,
        has_geometry: Optional[URIRef] = None,
        comment: Optional[Literal] = None,
        qualified_association: Optional[URIRef] = None,
        was_associated_with: Optional[URIRef] = None,
        has_site_visit: Union[SiteVisit, URIRef] = None,
        has_site: Union[Site, URIRef] = None,
        made_by_sampler: Optional[URIRef] = None,
        sampling_type: Optional[URIRef] = None,
    ):
        super(Sampling, self).__init__()
        self.assign_constructor_vars(locals())

    @staticmethod
    def generate_uri(namespace: Namespace, sample_id: str):
        return URIRef(namespace["sampling-{}".format(sample_id)])


class Sample(FeatureOfInterest, Result):
    class_uri = TERN.Sample
    dcterms_type = TERN.Sample

    mapping = {
        "dcterms_type": DCTERMS.type,  #
        "feature_type": TERN.featureType,  #
        "in_dataset": VOID.inDataset,  #
        "is_sample_of": MapTo(SOSA.isSampleOf, SOSA.hasSample),  #
        "is_result_of": MapTo(SOSA.isResultOf, SOSA.hasResult),  #
        "identifier": DCTERMS.identifier,  #
        "has_geometry": GEOSPARQL.hasGeometry,  #
        "qualified_association": PROV.qualifiedAssociation,
        "was_associated_with": PROV.wasAssociatedWith,
        "has_sample": MapTo(SOSA.hasSample, SOSA.isSampleOf),
        "comment": RDFS.comment,
        "is_feature_of_interest_of": SOSA.isFeatureOfInterestOf,
        "sf_within": GEOSPARQL.sfWithin,
        "conservation_status": TERN.conservationStatus,
    }

    feature_type: URIRef
    in_dataset: Union[RDFDataset, URIRef]
    is_result_of: Union[Sampling, URIRef]
    is_sample_of: Union[FeatureOfInterest, URIRef] = None
    identifier: Optional[Literal] = None
    is_feature_of_interest_of: Optional[URIRef] = None
    has_geometry: Optional[URIRef] = None
    comment: Optional[Literal] = None
    qualified_association: Optional[URIRef] = None
    was_associated_with: Optional[URIRef] = None
    has_sample: Optional[List[URIRef]] = None

    # TODO: Deprecated -> need fix
    sf_within: Optional[URIRef] = None
    conservation_status: Optional[Literal] = None

    def __init__(
        self,
        uri: URIRef,
        feature_type: URIRef,
        in_dataset: Union[RDFDataset, URIRef],
        is_result_of: Union[Sampling, URIRef],
        is_sample_of: Union[FeatureOfInterest, URIRef] = None,
        identifier: Optional[Literal] = None,
        is_feature_of_interest_of: Optional[URIRef] = None,
        has_geometry: Optional[URIRef] = None,
        comment: Optional[Literal] = None,
        qualified_association: Optional[URIRef] = None,
        was_associated_with: Optional[URIRef] = None,
        has_sample: Optional[List[URIRef]] = None,
        sf_within: Optional[URIRef] = None,
        conservation_status: Optional[Literal] = None,
    ):
        super(Sample, self).__init__(
            uri,
            in_dataset,
            feature_type,
            identifier,
            is_feature_of_interest_of=is_feature_of_interest_of,
            has_geometry=has_geometry,
            comment=comment,
        )
        self.assign_constructor_vars(locals())


class MaterialSample(Sample):
    class_uri = TERN.MaterialSample
    dcterms_type = TERN.MaterialSample

    mapping = {
        "dcterms_type": DCTERMS.type,
        "feature_type": TERN.featureType,
        "material_sample_id": DWCT.materialSampleID,
        "in_dataset": VOID.inDataset,
        "is_sample_of": MapTo(SOSA.isSampleOf, SOSA.hasSample),
        "is_result_of": MapTo(SOSA.isResultOf, SOSA.hasResult),
        "identifier": DCTERMS.identifier,
        "comment": RDFS.comment,
        "has_igsn": TERN.hasIGSN,
    }

    feature_type: URIRef
    material_sample_id: Union[URIRef, Literal]
    in_dataset: Union[RDFDataset, URIRef]
    is_sample_of: Union[FeatureOfInterest, URIRef]
    is_result_of: Union[Sampling, URIRef]
    identifier: Optional[Literal] = None
    comment: Optional[Literal] = None
    has_igsn: Optional[URIRef] = None

    def __init__(
        self,
        uri: URIRef,
        material_sample_id: Union[URIRef, Literal],
        feature_type: URIRef,
        in_dataset: URIRef,
        is_sample_of: Union[FeatureOfInterest, URIRef],
        is_result_of: Union[Sampling, URIRef],
        identifier: Literal = None,
        comment: Literal = None,
        has_igsn: Optional[URIRef] = None,
    ):
        super(MaterialSample, self).__init__(
            uri, feature_type, in_dataset, is_sample_of, is_result_of, identifier
        )
        self.assign_constructor_vars(locals())


class IGSN(RDFModel):

    class_uri = TERN.IGSN
    dcterms_type = TERN.IGSN

    mapping = {
        "identifier": DCTERMS.identifier,
        "creator": DCTERMS.creator,
        "title": DCTERMS.title,
        "publisher": DCTERMS.publisher,
        "publication_year": DCTERMS.issued,
        "landing_page": DCAT.landingPage,
        "resource_type": DCTERMS.type,
        "resource_type_general": TERN.sampleType,
        "subject": DCTERMS.subject,
        "contributer": DCTERMS.contributor,
        "date": DCTERMS.date,
        "alternate_identifier": ADMS.identifier,
        "related_identifier": MapTo(DCTERMS.isPartOf, DCTERMS.hasPart),
        "description": DCTERMS.description,
        "geo_location": GEOSPARQL.hasGeometry,
        "thumbnail_320": SCHEMA.contentUrl,
        "thumbnail_640": SCHEMA.contentUrl,
        "thumbnail_1280": SCHEMA.contentUrl,
        "thumbnail_original": SCHEMA.contentUrl,
    }

    identifier: Literal
    creator: Literal
    title: Literal
    publisher: Union[URIRef, Literal]
    publication_year: Literal
    landing_page: URIRef
    resource_type: Union[URIRef, Literal]
    resource_type_general: URIRef
    subject: Optional[Literal] = None
    contributer: Optional[Literal] = None
    date: Optional[Literal] = None
    alternate_identifier: Optional[Literal] = None
    related_identifier: Optional[Literal] = None
    description: Optional[Literal] = None
    geo_location: Optional[URIRef] = None
    thumbnail_320: Optional[str] = None
    thumbnail_640: Optional[str] = None
    thumbnail_1280: Optional[str] = None
    thumbnail_original: Optional[str] = None

    def __init__(
        self,
        uri: URIRef,
        identifier: Literal,
        creator: Literal,
        title: Literal,
        publisher: Literal,
        publication_year: Literal,
        landing_page: URIRef,
        resource_type: Union[URIRef, Literal],
        resource_type_general: URIRef,
        subject: Optional[Literal] = None,
        contributer: Optional[Literal] = None,
        date: Optional[Literal] = None,
        alternate_identifier: Optional[Literal] = None,
        related_identifier: Optional[Literal] = None,
        description: Optional[Literal] = None,
        geo_location: Optional[URIRef] = None,
    ):
        super(IGSN, self).__init__()
        self.assign_constructor_vars(locals())


class Boolean(Value):
    class_uri = TERN.Boolean

    mapping = {"value": RDF.value}

    value: Literal

    def __init__(
        self,
        uri: URIRef,
        value: Literal,
    ):
        super(Boolean, self).__init__()
        self.assign_constructor_vars(locals())


class Count(Value):
    class_uri = TERN.Count

    mapping = {
        "value": RDF.value,
        "uncertainty": TERN.uncertainty,
    }

    value: Literal
    uncertainty: Optional[Literal] = None

    def __init__(
        self,
        uri: URIRef,
        value: Literal,
        uncertainty: Optional[Literal] = None,
    ):
        super(Count, self).__init__()
        self.assign_constructor_vars(locals())


class Date(Value):
    class_uri = TERN.Date
    uri: URIRef

    mapping = {"value": RDF.value}

    value: Literal

    def __init__(self, uri: URIRef, value: Literal):
        super(Date, self).__init__()
        self.assign_constructor_vars(locals())


class DateTime(Value):
    class_uri = TERN.DateTime

    mapping = {"value": RDF.value}

    value: Literal

    def __init__(self, uri: URIRef, value: Literal):
        super(DateTime, self).__init__()
        self.assign_constructor_vars(locals())


class Text(Value):
    class_uri = TERN.Text

    mapping = {"value": RDF.value}

    value: Literal

    def __init__(
        self,
        uri: URIRef,
        value: Literal,
    ):
        super(Text, self).__init__()
        self.assign_constructor_vars(locals())


class Float(Value):
    class_uri = TERN.Float

    mapping = {
        "value": RDF.value,
        "uncertainty": TERN.uncertainty,
        "unit": TERN.unit,
    }

    value: Literal
    unit: Optional[URIRef] = None
    uncertainty: Optional[Literal] = None

    def __init__(
        self,
        uri: URIRef,
        value: Literal,
        unit: Optional[URIRef] = None,
        uncertainty: Optional[Literal] = None,
    ):
        super(Float, self).__init__()
        self.assign_constructor_vars(locals())


class Integer(Value):
    class_uri = TERN.Integer

    mapping = {
        "value": RDF.value,
        "uncertainty": TERN.uncertainty,
        "unit": TERN.unit,
    }

    value: Literal
    unit: URIRef
    uncertainty: Optional[Literal] = None

    def __init__(
        self,
        uri: URIRef,
        value: Literal,
        unit: URIRef,
        uncertainty: Optional[Literal] = None,
    ):
        super(Integer, self).__init__()
        self.assign_constructor_vars(locals())


class IRI(Value):
    class_uri = TERN.IRI

    mapping = {
        "value": RDF.value,
    }

    value: URIRef

    def __init__(
        self,
        uri: URIRef,
        value: URIRef,
    ):
        super(IRI, self).__init__()
        self.assign_constructor_vars(locals())


class Taxon(Value):
    class_uri = TERN.Taxon

    mapping = {
        "label": RDFS.label,
        "definition": SKOS.definition,
        "example": SKOS.example,
        "accepted_name_usage": DWCT.acceptedNameUsage,
        "accepted_name_usage_id": DWCT.acceptedNameUsageID,
        "_class": URIRef("http://rs.tdwg.org/dwc/terms/class"),
        "cultivar_epithet": DWCT.cultivarEpithet,
        "family": DWCT.family,
        "generic_name": DWCT.genericName,
        "genus": DWCT.genus,
        "higher_classification": DWCT.higherClassification,
        "infrageneric_epithet": DWCT.infragenericEpithet,
        "infraspecific_epithet": DWCT.infraspecificEpithet,
        "kingdom": DWCT.kingdom,
        "name_according_to": DWCT.nameAccordingTo,
        "name_according_to_id": DWCT.nameAccordingToID,
        "name_published_in": DWCT.namePublishedIn,
        "name_published_in_id": DWCT.namePublishedInID,
        "name_published_in_year": DWCT.namePublishedInYear,
        "nomenclatural_code": DWCT.nomenclaturalCode,
        "nomenclatural_status": DWCT.nomenclaturalStatus,
        "order": DWCT.order,
        "original_name_usage": DWCT.originalNameUsage,
        "original_name_usage_id": DWCT.originalNameUsageID,
        "parent_name_usage": DWCT.parentNameUsage,
        "parent_name_usage_id": DWCT.parentNameUsageID,
        "phylum": DWCT.phylum,
        "scientific_name": DWCT.scientificName,
        "scientific_name_authorship": DWCT.scientificNameAuthorship,
        "scientific_name_id": DWCT.scientificNameID,
        "specific_epithet": DWCT.specificEpithet,
        "subfamily": DWCT.subfamily,
        "subgenus": DWCT.subgenus,
        "taxon_concept_id": DWCT.taxonConceptID,
        "taxon_id": DWCT.taxonID,
        "taxon_rank": DWCT.taxonRank,
        "taxon_remarks": DWCT.taxonRemarks,
        "taxonomic_status": DWCT.taxonomicStatus,
        "verbatim_taxon_rank": DWCT.verbatimTaxonRank,
        "vernacular_name": DWCT.vernacularName,
    }

    label: Literal
    definition: Optional[Literal] = None
    example: Optional[Literal] = None
    accepted_name_usage: Optional[Literal] = None
    accepted_name_usage_id: Optional[Literal] = None
    _class: Optional[Literal] = None
    cultivar_epithet: Optional[Literal] = None
    family: Optional[Literal] = None
    generic_name: Optional[Literal] = None
    genus: Optional[Literal] = None
    higher_classification: Optional[Literal] = None
    infrageneric_epithet: Optional[Literal] = None
    infraspecific_epithet: Optional[Literal] = None
    kingdom: Optional[Literal] = None
    name_according_to: Optional[Literal] = None
    name_according_to_id: Optional[Literal] = None
    name_published_in: Optional[Literal] = None
    name_published_in_id: Optional[Literal] = None
    name_published_in_year: Optional[Literal] = None
    nomenclatural_code: Optional[Literal] = None
    nomenclatural_status: Optional[Literal] = None
    order: Optional[Literal] = None
    original_name_usage: Optional[Literal] = None
    original_name_usage_id: Optional[Literal] = None
    parent_name_usage: Optional[Literal] = None
    parent_name_usage_id: Optional[Literal] = None
    phylum: Optional[Literal] = None
    scientific_name: Optional[Literal] = None
    scientific_name_authorship: Optional[Literal] = None
    scientific_name_id: Optional[Literal] = None
    specific_epithet: Optional[Literal] = None
    subfamily: Optional[Literal] = None
    subgenus: Optional[Literal] = None
    taxon_concept_id: Optional[Literal] = None
    taxon_id: Optional[Literal] = None
    taxon_rank: Optional[Literal] = None
    taxon_remarks: Optional[Literal] = None
    taxonomic_status: Optional[Literal] = None
    verbatim_taxon_rank: Optional[Literal] = None
    vernacular_name: Optional[Literal] = None

    def __init__(
        self,
        uri: URIRef,
        label: Literal,
        definition: Optional[Literal] = None,
        example: Optional[Literal] = None,
        accepted_name_usage: Optional[Literal] = None,
        accepted_name_usage_id: Optional[Union[Literal, URIRef]] = None,
        _class: Optional[Literal] = None,
        cultivar_epithet: Optional[Literal] = None,
        family: Optional[Literal] = None,
        generic_name: Optional[Literal] = None,
        genus: Optional[Literal] = None,
        higher_classification: Optional[Literal] = None,
        infrageneric_epithet: Optional[Literal] = None,
        infraspecific_epithet: Optional[Literal] = None,
        kingdom: Optional[Literal] = None,
        name_according_to: Optional[Literal] = None,
        name_according_to_id: Optional[Union[Literal, URIRef]] = None,
        name_published_in: Optional[Literal] = None,
        name_published_in_id: Optional[Union[Literal, URIRef]] = None,
        name_published_in_year: Optional[Literal] = None,
        nomenclatural_code: Optional[Literal] = None,
        nomenclatural_status: Optional[Literal] = None,
        order: Optional[Literal] = None,
        original_name_usage: Optional[Literal] = None,
        original_name_usage_id: Optional[Union[Literal, URIRef]] = None,
        parent_name_usage: Optional[Literal] = None,
        parent_name_usage_id: Optional[Union[Literal, URIRef]] = None,
        phylum: Optional[Literal] = None,
        scientific_name: Optional[Literal] = None,
        scientific_name_authorship: Optional[Literal] = None,
        scientific_name_id: Optional[Union[Literal, URIRef]] = None,
        specific_epithet: Optional[Literal] = None,
        subfamily: Optional[Literal] = None,
        subgenus: Optional[Literal] = None,
        taxon_concept_id: Optional[Union[Literal, URIRef]] = None,
        taxon_id: Optional[Union[Literal, URIRef]] = None,
        taxon_rank: Optional[Literal] = None,
        taxon_remarks: Optional[Literal] = None,
        taxonomic_status: Optional[Literal] = None,
        verbatim_taxon_rank: Optional[Literal] = None,
        vernacular_name: Optional[Literal] = None,
    ):
        super(Taxon, self).__init__()
        self.assign_constructor_vars(locals())


class Instant(RDFModel):
    class_uri = TIME.Instant

    mapping = {
        "datetime": TIME.inDateTime,
    }

    datetime: Literal

    def __init__(self, uri: URIRef, datetime: Literal):
        super(Instant, self).__init__()
        self.assign_constructor_vars(locals())


class Attribute(RDFModel):
    class_uri = TERN.Attribute
    dcterms_type = TERN.Attribute

    mapping = {
        "dcterms_type": DCTERMS.type,
        "is_attribute_of": MapTo(TERN.isAttributeOf, TERN.hasAttribute),
        "in_dataset": VOID.inDataset,
        "attribute": TERN.attribute,
        "has_value": TERN.hasValue,
        "has_simple_value": TERN.hasSimpleValue,
    }

    is_attribute_of: Union[FeatureOfInterest, URIRef]
    in_dataset: URIRef
    attribute: URIRef
    has_value: Result
    has_simple_value: Literal

    def __init__(
        self,
        uri: URIRef,
        is_attribute_of: Union[FeatureOfInterest, URIRef],
        in_dataset: URIRef,
        attribute: URIRef,
        has_value: Value,
        has_simple_value: Literal,
    ):
        super(Attribute, self).__init__()
        self.assign_constructor_vars(locals())

    @staticmethod
    def generate_uri(
        namespace: Namespace, table_name: str, attribute_name: str, unique_id: Union[str, int]
    ) -> URIRef:
        return URIRef(
            namespace[
                "{table_name}-attr-{attribute_name}-{unique_id}".format(
                    table_name=table_name,
                    attribute_name=attribute_name,
                    unique_id=unique_id,
                ).lower()
            ]
        )


class Observation(RDFModel):
    class_uri = TERN.Observation
    dcterms_type = TERN.Observation

    mapping = {
        "dcterms_type": DCTERMS.type,
        "in_dataset": VOID.inDataset,
        "identifier": DCTERMS.identifier,
        "has_geometry": GEOSPARQL.hasGeometry,
        "comment": RDFS.comment,
        "has_result": MapTo(SOSA.hasResult, SOSA.isResultOf),
        "has_simple_result": SOSA.hasSimpleResult,
        "qualified_association": PROV.qualifiedAssociation,
        "was_associated_with": PROV.wasAssociatedWith,
        "feature_of_interest": SOSA.hasFeatureOfInterest,
        "made_by_sensor": SOSA.madeBySensor,
        "observed_property": SOSA.observedProperty,
        "phenomenon_time": SOSA.phenomenonTime,
        "result_datetime": TERN.resultDateTime,
        "used_procedure": SOSA.usedProcedure,
        "has_site_visit": MapTo(TERN.hasSiteVisit, TERN.isSiteVisitOf),
        "observation_type": TERN.observationType,
        "ecoplots_tags": URIRef("urn:ecoplots:tags"),
        "was_attributed_to": PROV.wasAttributedTo,
        "has_attribute": TERN.hasAttribute,
        "has_site": MapTo(TERN.hasSite, TERN.isSiteOf),
    }

    uri: URIRef
    feature_of_interest: Union[FeatureOfInterest, URIRef]
    in_dataset: URIRef
    has_result: Result
    has_simple_result: Literal
    observed_property: ObservableProperty
    used_procedure: URIRef
    phenomenon_time: Instant
    result_datetime: Literal
    identifier: Optional[Literal] = None
    has_geometry: Optional[URIRef] = None
    was_attributed_to: Optional[URIRef] = None
    has_attribute: Optional[Attribute] = None
    comment: Optional[Literal] = None
    observation_type: Optional[URIRef] = None
    qualified_association: Optional[URIRef] = None
    was_associated_with: Optional[URIRef] = None
    made_by_sensor: Optional[URIRef] = None
    has_site_visit: Optional[URIRef] = None
    ecoplots_tags: Optional[Literal] = None

    def __init__(
        self,
        uri: URIRef,
        feature_of_interest: Union[FeatureOfInterest, URIRef],
        in_dataset: URIRef,
        has_result: Result,
        has_simple_result: Literal,
        observed_property: ObservableProperty,
        used_procedure: URIRef,
        phenomenon_time: Instant,
        result_datetime: Literal,
        identifier: Optional[Literal] = None,
        has_geometry: Optional[Attribute] = None,
        was_attributed_to: Optional[URIRef] = None,
        has_attribute: Optional[Attribute] = None,
        comment: Optional[Literal] = None,
        has_site: Optional[URIRef] = None,
        has_site_visit: Optional[URIRef] = None,
        ecoplots_tags: Optional[Literal] = None,
        observation_type: Optional[URIRef] = None,
        qualified_association: Optional[URIRef] = None,
        was_associated_with: Optional[URIRef] = None,
        made_by_sensor: Optional[URIRef] = None,
    ):
        super(Observation, self).__init__()
        self.assign_constructor_vars(locals())

    @staticmethod
    def generate_uri(
        namespace: Namespace, table_name: str, parameter_name: str, unique_id: Union[str, int]
    ) -> URIRef:
        return URIRef(
            namespace[
                "{table_name}-obs-{parameter_name}-{unique_id}".format(
                    table_name=table_name,
                    parameter_name=parameter_name,
                    unique_id=unique_id,
                ).lower()
            ]
        )


class DerivedObservation(Observation):
    class_uri = TERN.DerivedObservation
    dcterms_type = TERN.DerivedObservation


class TERNDerivedObservation(DerivedObservation):
    class_uri = TERN.TERNDerivedObservation
    dcterms_type = TERN.TERNDerivedObservation


class Geometry(RDFModel):
    class_uri = TERN_LOC.Geometry
    dcterms_type = TERN_LOC.Geometry

    def __init__(self):
        super(Geometry, self).__init__()
        self.assign_constructor_vars(locals())


class Point(Geometry):
    class_uri = TERN_LOC.Point
    dcterms_type = TERN_LOC.Point

    mapping = {
        "dcterms_type": DCTERMS.type,
        "as_wkt": GEOSPARQL.asWKT,
        "point_type": TERN_LOC.pointType,
        "altitude": W3CGEO.alt,
        "latitude": W3CGEO.lat,
        "longitude": W3CGEO.long,
        "depth": TERN_LOC.depth,
        "elevation": TERN_LOC.elevation,
    }

    as_wkt: Literal
    point_type: Optional[URIRef] = None
    altitude: Optional[float] = None
    latitude: Optional[float] = None
    longitude: Optional[float] = None
    depth: Optional[float] = None
    elevation: Optional[float] = None

    def __init__(
        self,
        uri: URIRef,
        as_wkt: Literal,
        point_type: Optional[URIRef] = None,
        altitude: Optional[float] = None,
        latitude: Optional[float] = None,
        longitude: Optional[float] = None,
        depth: Optional[float] = None,
        elevation: Optional[float] = None,
    ):
        super(Point, self).__init__()
        self.assign_constructor_vars(locals())


class Polygon(Geometry):
    class_uri = TERN_LOC.Polygon
    dcterms_type = TERN_LOC.Polygon

    mapping = {
        "dcterms_type": DCTERMS.type,
        "as_wkt": GEOSPARQL.asWKT,
    }

    as_wkt: Literal

    def __init__(self, uri: URIRef, as_wkt: Literal):
        super(Polygon, self).__init__()
        self.assign_constructor_vars(locals())


class Person(RDFModel):
    class_uri = SCHEMA.Person
    dcterms_type = SCHEMA.Person

    mapping = {
        "name": SCHEMA.name,
    }

    name: Literal

    def __init__(
        self,
        uri: URIRef,
        name: Literal,
    ):
        super(Person, self).__init__()
        self.assign_constructor_vars(locals())


class Organization(RDFModel):
    class_uri = SCHEMA.Organization
    dcterms_type = SCHEMA.Organization

    mapping = {
        "name": SCHEMA.name,
    }

    name: Literal

    def __init__(
        self,
        uri: URIRef,
        name: Literal,
    ):
        super(Organization, self).__init__()
        self.assign_constructor_vars(locals())
