from datetime import datetime

from pyshacl import validate
from pyspark import Accumulator
from pyspark.sql import DataFrame
from pyspark.sql.functions import trim
from rdflib import Graph

from spark_etl_utils.rdf import parse_vocab_from_url
from spark_etl_utils.custom_accumulators import add_error

# Australia Boundary Box
AUS_LONGITUDE_MAX = 153.569469029
AUS_LONGITUDE_MIN = 113.338953078
AUS_LATITUDE_MAX = -10.6681857235
AUS_LATITUDE_MIN = -43.6345972634

# Queensland Boundary Box
QLD_LONGITUDE_MAX = 153.54
QLD_LONGITUDE_MIN = 137.9
QLD_LATITUDE_MAX = -9.22
QLD_LATITUDE_MIN = -30

SHAQCL_FILE = "https://raw.githubusercontent.com/ternaustralia/ontology_tern-plot/master/docs/plot.shapes.ttl"


def trim_whitespace(df: DataFrame, columns: list) -> DataFrame:
    """
    Remove the trailing whitespace from the front and back of a Spark DataFrame column.

    :param df: Spark DataFrame
    :param columns: A list of column names
    :return: Spark DataFrame
    """
    for col in columns:
        df = df.withColumn(col, trim(df[col]))
    return df


def get_string_columns(df: DataFrame) -> list:
    """
    Return a list of column names that contain string values.

    :param df: Spark DataFrame
    :return: List with the column names that are String
    """
    string_columns = []
    for col in df.dtypes:
        if 'string' in col[1]:
            string_columns.append(col[0])
    return string_columns


def get_timestamp_columns(df: DataFrame) -> list:
    """
    Return a list of column names that contain timestamp values.

    :param df: Spark DataFrame
    :return: List with the column names that are Timestamp
    """
    string_columns = []
    for col in df.dtypes:
        if 'timestamp' in col[1]:
            string_columns.append(col[0])
    return string_columns


def clean_columns(df: DataFrame) -> DataFrame:
    """
    Apply cleaning functions to the columns of a Spark DataFrame object. This is the entry point
    to the Validation package.

    :param df: Spark DataFrame
    :return: Spark DataFrame
    """
    # Clean columns that are Strings.
    if df is not None:
        string_columns = get_string_columns(df)
        df = trim_whitespace(df, string_columns)

    return df


def validate_dates(df: DataFrame) -> None:
    """
    Apply validation functions to the columns whose type is Datetime.
    :param df: Spark DataFrame
    :return:
    """
    if df is not None:
        timestamp_columns = get_timestamp_columns(df)
        datetime_with_timezone(df, timestamp_columns)


def has_timezone(date: datetime, column: str) -> None:
    if date.tzinfo is None:
        raise RuntimeError("Column '{}' has datetimes without Timezone: {}".format(column, date))


def datetime_with_timezone(df: DataFrame, columns: list) -> None:
    """
        Remove the trailing whitespace from the front and back of a Spark DataFrame column.

        :param df: Spark DataFrame
        :param columns: A list of column names
        :return: Spark DataFrame
        """
    for col1 in columns:
        df.foreach(lambda x: has_timezone(x[col1], col1))


def within_australia(df: DataFrame, latitude='latitude', longitude='longitude') -> None:
    """
    Check that the latitude and longitude coordinate values are within the bounding box of Australia.

    :param df: Spark DataFrame
    :param latitude: Latitude column name in the DataFrame
    :param longitude: Longitude column name in the DataFrame
    :return:
    """
    if df is not None:
        lat_df_test = df.filter((df[latitude] < AUS_LATITUDE_MIN) | (df[latitude] > AUS_LATITUDE_MAX))
        if len(lat_df_test.collect()) > 0:
            raise RuntimeError('The latitude coordinates are malformed. {}'.format(lat_df_test.collect()))

        long_df_test = df.filter((df[longitude] < AUS_LONGITUDE_MIN) | (df[longitude] > AUS_LONGITUDE_MAX))
        if len(long_df_test.collect()) > 0:
            raise RuntimeError('The longitude coordinates are malformed. {}'.format(long_df_test.collect()))


def within_queensland(df: DataFrame, latitude='latitude', longitude='longitude') -> None:
    """
    Check that the latitude and longitude coordinate values are within the bounding box of Queensland.

    :param df: Spark DataFrame
    :param latitude: Latitude column name in the DataFrame
    :param longitude: Longitude column name in the DataFrame
    :return:
    """
    if df is not None:
        lat_df_test = df.filter((df[latitude] < QLD_LATITUDE_MIN) | (df[latitude] > QLD_LATITUDE_MAX))
        if len(lat_df_test.collect()) > 0:
            raise RuntimeError('The latitude coordinates are malformed. {}'.format(lat_df_test.collect()))

        long_df_test = df.filter((df[longitude] < QLD_LONGITUDE_MIN) | (df[longitude] > QLD_LONGITUDE_MAX))
        if len(long_df_test.collect()) > 0:
            raise RuntimeError('The longitude coordinates are malformed. {}'.format(long_df_test.collect()))


def shacl_validator_file(g: Graph, shacl_shape: str, vocabs: Graph, table_name: str, errors: Accumulator):
    shacl_shape_graph = Graph().parse(data=shacl_shape, format="turtle")
    conforms, results_graph, results_text = validate(g, shacl_graph=shacl_shape_graph, ont_graph=vocabs,
                                                     inference='rdfs', abort_on_error=False,
                                                     meta_shacl=False, debug=False, data_graph_format='ttl',
                                                     shacl_graph_format='ttl')
    if not conforms:
        add_error(errors, "SHACL validation was unsuccessful for table: '{}'\n{}".format(table_name, results_text))
    return conforms


def shacl_validator(g: Graph, vocabs: Graph, table_name: str, errors: Accumulator):
    # TernRdf.Graph([AUSPLOTS_BINDINGS])
    shacl_shape_graph = parse_vocab_from_url(Graph(), SHAQCL_FILE)
    conforms, results_graph, results_text = validate(g, shacl_graph=shacl_shape_graph, ont_graph=vocabs,
                                                     inference='rdfs', abort_on_error=False,
                                                     meta_shacl=False, debug=False, data_graph_format='ttl',
                                                     shacl_graph_format='ttl')
    if not conforms:
        add_error(errors, "SHACL validation was unsuccessful for table: '{}'\n{}".format(table_name, results_text))
    return conforms
