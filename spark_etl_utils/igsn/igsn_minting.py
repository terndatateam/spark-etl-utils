import os
import requests
import json
from spark_etl_utils.database import get_db_query_pg
from pyspark.sql import SparkSession
from requests.auth import HTTPBasicAuth
from urllib.parse import quote

class IGSNMintingETL:
    def __init__(
        self,
        spark: SparkSession
    ) -> None:
        self.spark = spark
        self.db_host = os.getenv("POSTGRES_HOST")
        self.db_port = os.getenv("POSTGRES_PORT")
        self.db_name = os.getenv("POSTGRES_DB")
        self.db_username = os.getenv("POSTGRES_USER")
        self.db_password = os.getenv("POSTGRES_PASSWORD")
        self.target_db_host = os.getenv("TARGET_POSTGRES_HOST")
        self.target_db_port = os.getenv("TARGET_POSTGRES_PORT")
        self.target_db_name = os.getenv("TARGET_POSTGRES_DB")
        self.target_db_username = os.getenv("TARGET_POSTGRES_USER")
        self.target_db_password = os.getenv("TARGET_POSTGRES_PASSWORD")

    def extract_data(self, query: str, database: str="ecoplatform"):
        """
        Extracts data from the database using a provided SQL query.
        """
        df = get_db_query_pg(
            self.spark,
            self.db_host,
            self.db_port,
            self.db_name,
            query,
            self.db_username,
            self.db_password,
        ) if database == "ecoplatform" else (
            get_db_query_pg(
                self.spark,
                self.target_db_host,
                self.target_db_port,
                self.target_db_name,
                query,
                self.target_db_username,
                self.target_db_password,
            )
        )
        return df
    

    def save_igsn_result(self, df_igsn):
        """
        Save the DataFrame of IGSN results to the PostgreSQL database.
        
        :param df_igsn: Spark DataFrame containing the IGSN results.
        """
        df_igsn.write.jdbc(
            url=f"jdbc:postgresql://{self.target_db_host}:{self.target_db_port}/{self.target_db_name}",
            table="samples_igsn",
            mode="append",
            properties={
                "user": self.target_db_username,
                "password": self.target_db_password,
            },
        )

    def save_to_ecoplots_db(self, df, table):
        """
        Save the DataFrame of IGSN results to the PostgreSQL database.
        
        :param df_igsn: Spark DataFrame containing the IGSN results.
        """
        df.write.jdbc(
            url=f"jdbc:postgresql://{self.target_db_host}:{self.target_db_port}/{self.target_db_name}?stringtype=unspecified",
            table=table,
            mode="append",
            properties={
                "user": self.target_db_username,
                "password": self.target_db_password,
            },
        )

    # Function to mint or save draft for IGSN using DataCite API
    @staticmethod
    def mint_igsn(payload, mint=False):
        """
        Function to mint or save draft for IGSN using DataCite API with basic auth.
        Username and password are fetched from environment variables.
        :param payload: JSON payload for IGSN minting
        :param mint: Boolean flag to determine whether to mint the DOI or save it as a draft
        :return: Response from DataCite API
        """
        api_url = "https://api.test.datacite.org/dois"

        # Fetch the repository ID and password from environment variables
        repository_id = os.getenv('DATACITE_REPOSITORY_ID')
        password = os.getenv('DATACITE_PASSWORD')

        if not repository_id or not password:
            return {"error": "Missing DataCite credentials in environment variables."}

        # Update the payload based on the minting status
        if mint:
            payload['data']['attributes']['event'] = 'publish'  # To mint/publish the DOI
        else:
            payload['data']['attributes']['event'] = 'draft'  # To save the DOI as a draft

        headers = {
            'Content-Type': 'application/vnd.api+json' 
        }

        try:
            # Use basic authentication with the repository ID and password
            response = requests.post(
                api_url,
                headers=headers,
                data=json.dumps(payload),
                auth=HTTPBasicAuth(repository_id, password)
            )

            # Check if the request was successful
            if response.status_code in [201, 202]:  # 201 for mint, 202 for draft
                response_json = response.json()
                response_json["status_code"] = response.status_code
                return response_json
            else:
                return {
                    "error": "Failed to mint/save draft for IGSN",
                    "status_code": response.status_code,
                    "response": response.text
                }

        except Exception as e:
            return {
                "error": str(e)
            }
        
    @staticmethod
    def search_igsn(doi):
        """
        Searches for an IGSN DOI in DataCite.

        Args:
            doi (str): The DOI to search.

        Returns:
            dict: JSON response if DOI exists, otherwise an error message.
        """
        search_url = f"https://api.test.datacite.org/dois/{quote(doi, safe='')}"
        
        headers = {
            'Accept': 'application/vnd.api+json'
        }

        repository_id = os.getenv('DATACITE_REPOSITORY_ID')
        password = os.getenv('DATACITE_PASSWORD')

        try:
            response = requests.get(search_url, headers=headers, auth=HTTPBasicAuth(repository_id, password))

            if response.status_code == 200:
                response_json = response.json()
                response_json["status_code"] = 200
                return response_json
            else:
                return {
                    "error": f"DOI {doi} not found",
                    "status_code": response.status_code,
                    "response": response.text
                }

        except Exception as e:
            return {"error": str(e)}
