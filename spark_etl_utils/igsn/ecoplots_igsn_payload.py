from datetime import datetime
from typing import Dict

from rdflib import Graph, URIRef
from rdflib.graph import Seq as SeqRead
from rdflib.namespace import DCTERMS, RDF
from tern_rdf.namespace_bindings import SCHEMA


class IGSNPayloadConfig:
    def __init__(
        self,
        dataset: str,
        dataset_namespace: str,
        metadata_uri: str,
        output_dir: str,
        output_rdf_format: str,
        additional_fields: dict = None,
    ):
        """
        Initializes the Config class with parameters for dataset configuration.

        :param dataset: The dataset name.
        :param dataset_namespace: The dataset namespace URL.
        :param metadata_uri: The URI for metadata.
        :param output_dir: The output directory for saving results.
        :param output_rdf_format: The format of the output RDF.
        :param additional_fields: A dictionary of additional fields to be passed to the IGSN payload (key-value pairs).
        """
        self.DATASET = dataset
        self.DATASET_NAMESPACE = dataset_namespace
        self.METADATA_URI = metadata_uri
        self.OUTPUT_DIR = output_dir
        self.OUTPUT_RDF_FORMAT = output_rdf_format
        self.ADDITIONAL_FIELDS = additional_fields if additional_fields else {}

    def add_additional_fields_to_payload(self, payload: dict) -> None:
        """
        Adds additional fields from the config's additional_fields dictionary to the payload.

        :param payload: The payload dictionary to which the additional fields will be added.
        """
        if self.ADDITIONAL_FIELDS:
            for field, value in self.ADDITIONAL_FIELDS.items():
                payload["data"]["attributes"][field] = value


# Function to extract and format creators for IGSN payload
def creator_generation(output_g, dataset_namespace):
    """
    Generates a list of creators from the RDF graph.

    Args:
        output_g (rdflib.Graph): The RDF graph containing creator information.
        dataset_namespace (str): The namespace URI of the dataset.

    Returns:
        list: A list of creator dictionaries formatted for IGSN payload.
    """
    creators = []

    creator_properties = [DCTERMS.creator, URIRef("https://w3id.org/tern/ontologies/tern/coAuthor")]

    # Loop through each creator property (dct:creator and tern:coAuthor)
    for creator_prop in creator_properties:
        for creator_seq in output_g.objects(URIRef(dataset_namespace), creator_prop):
            if (creator_seq, RDF.type, RDF.Seq) in output_g:
                # If it's an rdf:Seq, read the sequence properly
                seq = SeqRead(output_g, creator_seq)

                for creator in seq:
                    creator_name = output_g.value(creator, SCHEMA.name)
                    orcid = str(creator) if "orcid" in str(creator) else None

                    if creator_name and "Terrestrial Ecosystem Research Network" in creator_name:
                        creator_obj = {
                            "name": "Terrestrial Ecosystem Research Network",
                            "nameType": "Organizational",
                            "nameIdentifiers": [
                                {
                                    "nameIdentifier": "03wxseg04",
                                    "nameIdentifierScheme": "ROR",
                                    "schemeURI": "https://ror.org",
                                }
                            ],
                        }
                        creators.append(creator_obj)
                        continue

                    # Determine if the creator is an Organization or Person
                    creator_type = output_g.value(creator, RDF.type)
                    name_type = (
                        "Organizational" if "Organization" in str(creator_type) else "Personal"
                    )

                    # Create the IGSN formatted creator object
                    creator_obj = {"name": str(creator_name), "nameType": name_type}

                    if orcid:
                        creator_obj["nameIdentifiers"] = [
                            {
                                "nameIdentifier": orcid.split("/")[-1],
                                "nameIdentifierScheme": "ORCID",
                                "schemeURI": "https://orcid.org",
                            }
                        ]

                    creators.append(creator_obj)
            else:
                # Handle if it's a single creator and not a sequence
                creator_name = output_g.value(creator_seq, SCHEMA.name)
                orcid = str(creator_seq) if "orcid" in str(creator_seq) else None

                if creator_name and "Terrestrial Ecosystem Research Network" in creator_name:
                    creator_obj = {
                        "name": "Terrestrial Ecosystem Research Network",
                        "nameType": "Organizational",
                        "nameIdentifiers": [
                            {
                                "nameIdentifier": "03wxseg04",
                                "nameIdentifierScheme": "ROR",
                                "schemeURI": "https://ror.org",
                            }
                        ],
                    }
                    creators.append(creator_obj)
                else:
                    # Determine if the creator is an Organization or Person
                    creator_type = output_g.value(creator_seq, RDF.type)
                    name_type = (
                        "Organizational" if "Organization" in str(creator_type) else "Personal"
                    )

                    # Create the IGSN formatted creator object
                    creator_obj = {"name": str(creator_name), "nameType": name_type}

                    if orcid:
                        creator_obj["nameIdentifiers"] = [
                            {
                                "nameIdentifier": orcid.split("/")[-1],
                                "nameIdentifierScheme": "ORCID",
                                "schemeURI": "https://orcid.org",
                            }
                        ]

                    creators.append(creator_obj)

    # Return the formatted creators list
    return creators


# Function to generate the IGSN payload based on the config and identifier
def generate_igsn_payload(
    creators: list,
    url: str,
    title: str,
    resource_type: str,
    prefix: str,
    suffix: bool = False,
    identifier: str = None,
    **kwargs
) -> dict:
    # Call the get_party_from_metadata_record function using config attributes
    # output_g = get_party_from_metadata_record(
    #     config.DATASET,
    #     config.DATASET_NAMESPACE,
    #     config.METADATA_URI,
    #     config.OUTPUT_DIR,
    #     config.OUTPUT_RDF_FORMAT
    # )

    # file_pattern = "../output/party_role_citation-*.ttl"

    # # Use glob to find the file matching the pattern
    # files = glob.glob(file_pattern)

    # # Check if any file matches the pattern
    # if not files:
    #     raise FileNotFoundError(f"No files found matching the pattern: {file_pattern}")

    # # get the first file
    # file_path = files[0]

    # # Load the RDF graph from the file
    # output_g = Graph()
    # output_g.parse(file_path, format="turtle")

    # IGSN payload dictionary
    igsn_payload = {"data": {"type": "dois", "attributes": {}}}

    # Handle identifier (mandatory field)
    # e.g. 10.83032/SAA028682
    if identifier:
        igsn_payload["data"]["attributes"]["doi"] = identifier
        # Handle title (derived from the identifier)
    elif suffix:
        igsn_payload["data"]["attributes"]["prefix"] = prefix
    else:
        raise ValueError("Either 'identifier' or 'suffix=True' must be provided")

    # Handle title (derived from the identifier)
    igsn_payload["data"]["attributes"]["titles"] = [{"title": title}]  # TODO: the actual title

    # Handle creators
    # creators = creator_generation(output_g, config.DATASET_NAMESPACE)
    igsn_payload["data"]["attributes"]["creators"] = creators

    # Publisher is always the same
    igsn_payload["data"]["attributes"]["publisher"] = {
        "lang": "en",
        "name": "Terrestrial Ecosystem Research Network (TERN)",
        "schemeUri": "ROR",
        "publisherIdentifier": "03wxseg04",
        "publisherIdentifierScheme": "https://ror.org/",
    }

    # Current year
    igsn_payload["data"]["attributes"]["publicationYear"] = datetime.now().year

    # TODO: Resource Type Vocab by Query
    igsn_payload["data"]["attributes"]["types"] = {"resourceTypeGeneral": resource_type}

    # Add the URL
    # e.g. "https://ecoplots-test.tern.org.au/sample/SAA028682"
    igsn_payload["data"]["attributes"]["url"] = url

    # Add additional fields dynamically from kwargs
    for field_name, field_value in kwargs.items():
        igsn_payload["data"]["attributes"][field_name] = field_value

    return igsn_payload
