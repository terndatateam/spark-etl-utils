import os
from io import BytesIO
from pathlib import Path

import requests
import swiftclient
from PIL import Image
from pyspark.sql.types import FloatType, MapType, StringType, StructField, StructType, TimestampType

PLANT_TISSUE_SAMPLE = {
    "name": "Plant Tissue Sample",
    "url": "http://linked.data.gov.au/def/tern-cv/eee45444-7940-4ff5-801f-8eef5452943f",
}

PLANT_VOUCHER_SPECIMEN = {
    "name": "Plant Voucher Specimen",
    "url": "http://linked.data.gov.au/def/tern-cv/18317af1-7c83-468d-883e-ba791500c6e3",
}

SOIL_METAGENOMICS_SAMPLE = {
    "name": "Soil Metagenomics Sample",
    "url": "http://linked.data.gov.au/def/tern-cv/f69d8f1e-d83f-49b1-be06-6553b04914fc",
}

SOIL_PIT_SAMPLE = {
    "name": "Soil Pit Sample",
    "url": "http://linked.data.gov.au/def/tern-cv/a49f0fcd-19f5-4098-ac2e-85a972286a43",
}

SOIL_SUBPIT_SAMPLE = {
    "name": "Soil Subpit Sample",
    "url": "http://linked.data.gov.au/def/tern-cv/f51d6ffa-6108-4aab-b63b-b41fc7748da6",
}

PRIMARY_GENETIC_SUBTYPE = {
    "name": "primary genetic",
    "url": "http://linked.data.gov.au/def/tern-cv/6bd7fb09-d34e-48e6-9f1e-4aca0dd9ff8d",
}

SECONDARY_GENETIC_SUBTYPE_1 = {
    "name": "secondary genetic 1",
    "url": "http://linked.data.gov.au/def/tern-cv/00ff09f1-9701-4c5a-b543-16cb8d54b86f",
}

SECONDARY_GENETIC_SUBTYPE_2 = {
    "name": "secondary genetic 2",
    "url": "http://linked.data.gov.au/def/tern-cv/a5c1f1e5-dff6-45d4-8e6a-0e6027fb4cdb",
}

SECONDARY_GENETIC_SUBTYPE_3 = {
    "name": "secondary genetic 3",
    "url": "http://linked.data.gov.au/def/tern-cv/24d1fa70-d689-4c19-ab6b-0b0c5c2130d5",
}

SECONDARY_GENETIC_SUBTYPE_4 = {
    "name": "secondary genetic 4",
    "url": "http://linked.data.gov.au/def/tern-cv/d7b10133-752e-44f1-b2fb-61bd7950c533",
}

ZERO_TO_TEN_SUBTYPE = {
    "name": "zero-to-ten centimeters",
    "url": "http://linked.data.gov.au/def/tern-cv/6cb5a5bb-f229-4f5f-9525-e8a489db15e1",
}

TEN_TO_TWENTY_SUBTYPE = {
    "name": "ten-to-twenty centimeters",
    "url": "http://linked.data.gov.au/def/tern-cv/21636062-dc82-46fd-83f4-92f362bee1e0",
}

TWENTY_TO_THIRTY_SUBTYPE = {
    "name": "twenty-to-thirty centimeters",
    "url": "http://linked.data.gov.au/def/tern-cv/8fa96d22-dc7f-425b-af9e-da46c7ef9cdf",
}

SOIL_METAGENOMICS_SUBTYPE = {
    "name": "soil metagenomics",
    "url": "http://linked.data.gov.au/def/tern-cv/df234330-e0fc-46b9-902f-988c9eddea07",
}

IGSN_SCHEMA = StructType(
    [
        StructField("sample_id", StringType(), False),
        StructField("sample_id_encoded", StringType(), False),
        StructField("doi", StringType(), True),
        StructField("doi_full", StringType(), True),
        StructField("doi_status", StringType(), True),
        StructField("sample_type", StringType(), True),
        StructField("sample_type_uri", StringType(), True),
        StructField("sample_subtype", StringType(), True),
        StructField("sample_subtype_uri", StringType(), True),
        StructField("landing_page_url", StringType(), True),
        StructField("metadata_payload", MapType(StringType(), StringType()), False),
        StructField("created_at", TimestampType(), True),
        StructField("modified_at", TimestampType(), True),
        StructField("published_at", TimestampType(), True),
        StructField("latitude", FloatType(), True),
        StructField("longitude", FloatType(), True),
    ]
)

SAMPLE_IMAGE_SCHEMA = StructType(
    [
        StructField("sample_id", StringType(), True),
        StructField("sample_id_encoded", StringType(), True),
        StructField("bioscanner_url", StringType(), True),
        StructField("status", StringType(), True),
        StructField("images", MapType(StringType(), StringType()), True),
        StructField("created_at", TimestampType(), True),
        StructField("modified_at", TimestampType(), True),
    ]
)


def get_auth_token():
    url = os.getenv("OS_AUTH_URL")
    payload = {
        "auth": {
            "identity": {
                "methods": ["password"],
                "password": {
                    "user": {
                        "name": os.getenv("OS_USERNAME"),
                        "domain": {"name": os.getenv("OS_USER_DOMAIN_NAME")},
                        "password": os.getenv("OS_PASSWORD"),
                    }
                },
            },
            "scope": {
                "project": {
                    "name": os.getenv("OS_PROJECT_NAME"),
                    "domain": {"name": os.getenv("OS_PROJECT_DOMAIN_NAME")},
                }
            },
        }
    }

    headers = {"Content-Type": "application/json"}
    response = requests.post(f"{url}/auth/tokens", json=payload, headers=headers)

    if response.status_code == 201:
        return response.headers["X-Subject-Token"]
    else:
        raise Exception(f"Failed to obtain auth token: {response.status_code} {response.text}")


def create_thumbnails(image_url, sample_id):
    """
    Creates thumbnails of various sizes for the given image, converts them to JPEG, and uploads them to the Swift object store.

    :param image_url: URL of the original image.
    :param sample_id: Sample ID for naming the files.
    :return: Dictionary of URLs of the uploaded thumbnails in the Swift object store.
    """
    try:
        # Get auth token for nectar
        auth_token = get_auth_token()

        # Retrieve the original image using the token
        headers = {"X-Auth-Token": auth_token}
        response = requests.get(image_url, headers=headers)

        if response.status_code != 200:
            raise Exception(
                f"Failed to download image from {image_url}. Status code: {response.status_code}"
            )

        original_image = Image.open(BytesIO(response.content))

        # Initialize Swift client
        swift_conn = swiftclient.Connection(
            authurl=os.getenv("OS_AUTH_URL"),
            user=os.getenv("OS_USERNAME"),
            key=os.getenv("OS_PASSWORD"),
            auth_version="3",
            os_options={
                "project_name": os.getenv("OS_PROJECT_NAME"),
                "user_domain_name": os.getenv("OS_USER_DOMAIN_NAME"),
                "project_domain_name": os.getenv("OS_PROJECT_DOMAIN_NAME"),
            },
        )

        sizes = {
            "thumbnail_320": (320, 320),
            "thumbnail_640": (640, 640),
            "thumbnail_1280": (1280, 1280),
            # "thumbnail_original": None,
            "original": None,
        }

        urls = {}

        # Extract the filename from the URL (without the extension)
        image_name_orig = Path(image_url).stem

        for suffix, size in sizes.items():
            image = original_image.copy()

            if size:
                image.thumbnail(size)
                quality = 85  # Compressed thumbnails
            else:
                quality = 60 if suffix == "thumbnail_original" else 100

            image_name = f"{sample_id}/{image_name_orig}_{suffix}.webp"

            if suffix != "original":
                # Save the image to a binary stream in WebP format
                image_stream = BytesIO()
                image.save(image_stream, format="WEBP", quality=quality)
                image_stream.seek(0)

                # Upload the image to Swift
                container_name = os.getenv("DEST_ROOT")
                swift_conn.put_object(
                    container=container_name,
                    obj=image_name,
                    contents=image_stream,
                    content_type="image/webp",
                )
            else:
                # Upload the original image to Swift, without any modification
                container_name = os.getenv("DEST_ROOT")
                swift_conn.put_object(
                    container=container_name,
                    obj=image_name,
                    contents=BytesIO(response.content),
                    content_type="image/webp",
                )

            storage_url = os.getenv("OS_STORAGE_URL")
            swift_url = f"{storage_url}/{container_name}/{image_name}"
            urls[suffix] = swift_url

        return urls

    except Exception as e:
        print(f"Error during thumbnail creation or upload: {str(e)}")
        return {}
