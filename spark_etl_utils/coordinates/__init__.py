from pyproj import Proj
from .species_conservation_status import *


def utm_to_lat_long(zone, easting, northing, ellps="GRS80", units="m"):
    options = {"proj": "utm", "zone": zone, "south": True, "ellps": ellps, "units": units}
    projection = Proj(options)
    lon, lat = projection(easting, northing, inverse=True)
    return lat, lon
