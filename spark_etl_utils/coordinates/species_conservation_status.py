import math
import random
from typing import Tuple


def random_point_in_disk(radius: float, lat: float, lon: float) -> Tuple[float]:
    # Implementation inspired by Stack Overflow: https://stackoverflow.com/a/43202522
    theta = random.uniform(0, 1) * 2 * math.pi
    r = math.sqrt(random.uniform(0, 1)) * radius
    dx = math.cos(theta) * r
    dy = math.sin(theta) * r

    earth_radius = 6371  # km
    one_degree = earth_radius * 2 * math.pi / 360 * 1000  # 1° latitude in meters

    return (lat + dy / one_degree, lon + dx / (one_degree * math.cos(lat * math.pi / 180)))


def generalise_coordinates(generalisation, orig_lat, orig_long):
    if generalisation == "10km":
        lat, long = random_point_in_disk(10000, orig_lat, orig_long)
        return lat, long, orig_lat - lat, orig_long - long
    elif generalisation == "1km":
        lat, long = random_point_in_disk(1000, orig_lat, orig_long)
        return lat, long, orig_lat - lat, orig_long - long
    else:
        return orig_lat, orig_long, 0, 0


def generalise_latitude_for_udf(generalisation, orig_lat, orig_long):
    if generalisation == "10km":
        lat, _ = random_point_in_disk(10000, orig_lat, orig_long)
        return lat
    elif generalisation == "1km":
        lat, _ = random_point_in_disk(1000, orig_lat, orig_long)
        return lat
    else:
        return orig_lat


def generalise_longitude_for_udf(generalisation, orig_lat, orig_long):
    if generalisation == "10km":
        _, long = random_point_in_disk(10000, orig_lat, orig_long)
        return long
    elif generalisation == "1km":
        _, long = random_point_in_disk(1000, orig_lat, orig_long)
        return long
    else:
        return orig_long


def state_from_uri(uri):
    # Based on vocabulary: "http://linked.data.gov.au/dataset/asgs2016/stateorterritory/*
    if uri == "1":
        return "NSW"
    elif uri == "2":
        return "VIC"
    elif uri == "3":
        return "QLD"
    elif uri == "4":
        return "SA"
    elif uri == "5":
        return "WA"
    elif uri == "6":
        return "TAS"
    elif uri == "7":
        return "NT"
    elif uri == "8":
        return "ACT"
    else:
        "INVALID_STATE_CODE"


def state_from_ausplots_id(code):
    # Based on ausplots/forest site name
    if code == "NS":
        return "NSW"
    elif code == "VC":
        return "VIC"
    elif code == "QD":
        return "QLD"
    elif code == "SA":
        return "SA"
    elif code == "WA":
        return "WA"
    elif code == "TC":
        return "TAS"
    elif code == "NT":
        return "NT"
    elif code == "CT":
        return "ACT"
    else:
        "INVALID_STATE_CODE"
