import inspect

from pyspark import AccumulatorParam, Accumulator
from rdflib import Graph


class RDFGraphAccumulator(AccumulatorParam):
    """
    Custom Spark Accumulator class to *accumulate* triples in an RDFLib Graph.
    """

    def zero(self, g):
        return g

    def addInPlace(self, value1, value2):
        if isinstance(value2, Graph):
            # value1 is now the default value,
            # return value2 instead (the accumulated Graph object)
            return value2

        # Add (value2) as a set of triples to (value1) Graph
        value1.add(value2)
        return value1


class ListAccumulator(AccumulatorParam):
    """
    Custom Spark Accumulator class to *accumulate* things in a Python List.
    """

    def zero(self, value):
        return value

    def addInPlace(self, value1, value2):
        if isinstance(value2, list):
            return value1 + value2

        value1.append(value2)
        return value1


def add_error(errors: Accumulator, msg: str) -> None:
    """
    Add a string error message to a Spark Accumulator.

    :param errors: pyspark.Accumulator
    :param msg: str message
    """

    curframe = inspect.currentframe()
    calframe = inspect.getouterframes(curframe, 2)
    errors.add('Function {} - {}'.format(calframe[1][3], msg))
