# Spark ETL Utils 
A Python library with multiple classes and methods common to several TERN (Apache Spark) ETL programs.

## Installation
Install using pip / include it in a requirements.txt file:

```
pip install --upgrade git+https://bitbucket.org/terndatateam/spark-etl-utils.git@master
```
Using the --upgrade option we insure that any small change or fix in the package that was not resulting in a new version is available.

## Example usage


## Notes

#### Converting UTM to Latitude Longitude
Some coordinates in the dataset are given in UTM (Universal Transverse Mercator) format, so they are converted to Latitude and Longitude coordinates to fit the plot:Location
scheme definition. 

[Pyproj](https://pyproj4.github.io/pyproj/stable/) library, a python interface to [PROJ](https://proj.org/) (cartographic projections and coordinate transformations library) is used to perform this conversion.

The library is pretty straightforward, as it is shown in the following example of use:

```
# Set up the Projection options. 
# Note that we are in the southern hemisphere and it is to be used the 'GRS80' ellipsoid.
proj_options = {
    'proj': 'utm',
    'zone': row[ZONE],
    'south': True,
    'ellps': 'GRS80',
    'units': 'm'
}
myProj = Proj(proj_options)

# Note that inverse parameter is set to True to perform the conversion in 
# the UTM -> Lat/Lon direction.
lon, lat = myProj(row[EASTING], row[NORTHING], inverse=True)
```


## Improvements

## Contact
**Javier Sanchez**  
*Senior Software Engineer*  
[j.sanchezgonzalez@uq.edu.au](mailto:j.sanchezgonzalez@uq.edu.au)  

